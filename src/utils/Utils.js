import React from 'react';
import { Redirect } from 'react-router-dom';
import { Icon } from '../components/Component';
import { toast } from 'react-toastify';
import PropTypes from 'prop-types';
import classNames from 'classnames';

/**
 * CloseButton component.
 * @returns {JSX.Element} The CloseButton component.
 */
const CloseButton = () => {
    return (
        <span className="btn-trigger toast-close-button" role="button">
            <Icon name="cross"></Icon>
        </span>
    );
};

/**
 * Displays a toast message.
 *
 * @param {string} type - The type of the toast message.
 * @param {string} message - The content of the toast message.
 * @param {function} onCloseCallback - The callback function to be executed when the toast is closed.
 */
const showToast = (type, message, onCloseCallback) => {
    const options = {
        position: 'top-right',
        autoClose: 1500,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: false,
        closeButton: <CloseButton />,
        onClose: onCloseCallback // Pass the callback to onClose
    };

    // Check if the specified type exists as a method on the toast object
    if (toast[type]) {
        toast[type](message, options);
    } else {
        // Handle the case where an invalid type is provided
        toast.error(message, options);
    }
};

/**
 * Renders a redirect component with a 404 state.
 * @param {Object} props - The component props.
 * @param {Object} props.location - The current location object.
 * @returns {JSX.Element} - The redirect component.
 */
const RedirectAs404 = ({ location }) => (
    <Redirect to={Object.assign({}, location, { state: { is404: true } })} />
);

RedirectAs404.propTypes = {
    location: PropTypes.object.isRequired
};

// Function to generate a sequential employee id for a new employee starting EMP0001
/**
 * Generates an employee ID based on the number of existing users.
 * @param {number} usersCount - The number of existing users.
 * @returns {string} The generated employee ID.
 */
const generateEmployeeId = (usersCount) => {
    const id = usersCount + 1;
    const idString = id.toString();
    const idLength = idString.length;
    const prefix = 'EMP';
    const zeros = '0'.repeat(4 - idLength);
    return prefix + zeros + idString;
};

/**
 * CSS class names for the form element.
 * @type {string}
 */
const formClass = classNames({
    'form-validate': true,
    'is-alter': true
});

export { generateEmployeeId, showToast, RedirectAs404, CloseButton, formClass };
