import React, { useState, useEffect } from 'react';
import Content from '../../layout/content/Content';
import Head from '../../layout/head/Head';
import {
    DropdownMenu,
    DropdownToggle,
    UncontrolledDropdown,
    DropdownItem
} from 'reactstrap';
import {
    Block,
    BlockBetween,
    BlockHead,
    BlockHeadContent,
    Icon,
    Row,
    Col,
    PaginationComponent,
    DataTable,
    DataTableBody,
    DataTableHead,
    DataTableRow,
    DataTableItem,
    Button,
    RSelect
} from '../../components/Component';
import { Link, useHistory } from 'react-router-dom';
import {
    bulkActionOptions,
    APP_URL,
    statuses,
    capitalize, formatDate
} from '../../utils/Constants';
import { showToast } from '../../utils/Utils';
import Swal from 'sweetalert2';
import axios from 'axios';
import { ToastContainer } from 'react-toastify';
import DatePicker from 'react-datepicker';
import { useTitle } from '../../contexts/TitleContext';
import PropTypes from 'prop-types';

const Index = ({ pageTitle }) => {
    const history = useHistory();
    const [originalData, setOriginalData] = useState([]);
    const [data, setData] = useState([]);
    const [tablesm, updateTableSm] = useState(false);
    const [onSearch, setonSearch] = useState(true);
    const [actionText, setActionText] = useState('');
    const [currentPage, setCurrentPage] = useState(1);
    const [itemPerPage, setItemPerPage] = useState(10);
    const [sort, setSortState] = useState('');
    const [onSearchText, setSearchText] = useState('');

    // State for filters
    const [appliedFilters, setAppliedFilter] = useState({
        start: null,
        end: null,
        status: ''
    });

    const { setDocumentTitle } = useTitle();

    useEffect(() => {
        setDocumentTitle(pageTitle);
    }, [pageTitle, setDocumentTitle]);

    // Function to get all the brands
    const getData = async () => {
        let response;

        try {
            response = await axios.get(`${APP_URL}/brands`);
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }

        setData(response.data.brands);
        setOriginalData(response.data.brands);
    };

    // Function to get all the brands on page load
    useEffect(() => {
        getData();
    }, []);

    // Function to delete a category
    const onDeleteClick = (id) => {
        Swal.fire({
            title: 'Are you sure?',
            text: 'You won\'t be able to revert this!',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!'
        }).then(async (result) => {
            if (result.isConfirmed) {
                try {
                    const result = await axios.delete(
                        `${APP_URL}/brands/delete/${id}`
                    );
                    showToast('success', result.data.message);
                    getData();
                } catch (error) {
                    showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
                }
            }
        });
    };

    // onChange function for date range filter
    const onDateRangeFilterChange = (dates) => {
        const [start, end] = dates;
        // setAppliedFilter({ ...appliedFilters, start, end });
        setAppliedFilter(prevState => ({ ...prevState, start, end }));
    };

    // Function to toggle the search option
    const toggleSearchFilter = () => setonSearch(!onSearch);

    // Changing state value when searching name
    useEffect(() => {
        if (onSearchText !== '') {
            const filteredObject = originalData.filter((item) => {
                return (
                    item.name
                        .toLowerCase()
                        .includes(onSearchText.toLowerCase()) ||
                    item.status
                        .toLowerCase()
                        .includes(onSearchText.toLowerCase())
                );
            });
            setData([...filteredObject]);
        } else {
            getData();
        }
    }, [onSearchText, setData]);

    // onChange function for status filter
    const onStatusFilterChange = (status) => {
        console.log(status);
        // setAppliedFilter({...appliedFilters, status});
        setAppliedFilter(prevState => ({ ...prevState, status }));
    };

    useEffect(() => {
        // Apply date range filter
        if (appliedFilters.start !== null && appliedFilters.end !== null) {
            console.log('appliedFilters start and end');

            const filteredData = originalData.filter((item) => {
                const createdAt = new Date(item.createdAt);

                return (
                    createdAt >= appliedFilters.start &&
                    createdAt <= appliedFilters.end
                );
            });

            setData(filteredData);
        }

        // Apply status filter
        if (appliedFilters.status !== '') {
            const filteredData = originalData.filter((item) => {
                return item.status === appliedFilters.status;
            });
            setData(filteredData);
        }
    }, [appliedFilters, setData]);

    const resetFilter = () => {
        setAppliedFilter({
            start: null,
            end: null,
            status: ''
        });
    };

    // Function to sort the data based on name
    const onSort = (params) => {
        const defaultData = data;
        if (params === 'asc') {
            const sortedData = defaultData.sort((a, b) =>
                a.name.localeCompare(b.name)
            );
            setData([...sortedData]);
        } else if (params === 'dsc') {
            const sortedData = defaultData.sort((a, b) =>
                b.name.localeCompare(a.name)
            );
            setData([...sortedData]);
        }
    };

    // Get current list, pagination
    const indexOfLastItem = currentPage * itemPerPage;
    const indexOfFirstItem = indexOfLastItem - itemPerPage;
    const currentItems = data.slice(indexOfFirstItem, indexOfLastItem);

    // Change Page
    const paginate = (pageNumber) => setCurrentPage(pageNumber);

    // Function to change the selected property of an item
    const onSelectChange = (e, id) => {
        const newData = data;
        const index = newData.findIndex((item) => item._id === id);
        newData[index].checked = e.currentTarget.checked;
    };

    // Function which selects all the items
    const onSelectAllChange = (e) => {
        data.map((item) => {
            item.checked = e.currentTarget.checked;
            return item;
        });

        setData([...data]);
    };

    // Function to set the action to be taken in table header
    const onBulkActionDropdownChange = (e) => {
        setActionText(e.value);
    };

    // function which fires on applying selected action
    const onBulkActionButtonClick = async (e) => {
        // Get all the checked data's id and store it in an array
        let checkedData = data.filter((item) => item.checked === true);
        checkedData = checkedData.map((item) => item._id);

        try {
            await axios.post(`${APP_URL}/brands/bulk-action`, {
                action: actionText,
                brandIds: checkedData
            });

            // Fetching data from api
            getData();
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }

        resetBulkActionAndCheckedRows();
    };

    // Function to reset the selected data and action
    const resetBulkActionAndCheckedRows = () => {
        // Uncheck checkbox with an id of uid
        const checkbox = document.getElementById('uid');
        checkbox.checked = false;
    };

    return (
        <React.Fragment>
            <Head title={pageTitle}></Head>
            <Content>
                <BlockHead size="sm">
                    <BlockBetween>
                        <BlockHeadContent>
                            <ul className="nk-block-tools g-3">
                                <li className="nk-block-tools-opt">
                                    <Button
                                        color="primary"
                                        className="toggle d-none d-md-inline-flex"
                                        onClick={() =>
                                            history.push(
                                                `${process.env.PUBLIC_URL}/account/brands/create`
                                            )
                                        }
                                    >
                                        <Icon name="plus"></Icon>
                                        {/* Add Employee */}
                                        <span>Add Brand</span>
                                    </Button>
                                </li>
                                <li>
                                    <a
                                        href="#export"
                                        onClick={(ev) => {
                                            ev.preventDefault();
                                        }}
                                        className="btn btn-white btn-outline-light"
                                    >
                                        <Icon name="download-cloud"></Icon>
                                        <span>Export</span>
                                    </a>
                                </li>
                            </ul>
                        </BlockHeadContent>
                    </BlockBetween>
                </BlockHead>

                <Block>
                    <DataTable className="card-stretch">
                        {/* START: Table Header */}
                        <div className="card-inner position-relative card-tools-toggle">
                            <div className="card-title-group">
                                <div className="card-tools">
                                    <div className="form-inline flex-nowrap gx-3">
                                        <div className="form-wrap">
                                            <RSelect
                                                options={bulkActionOptions}
                                                className="w-130px"
                                                placeholder="Bulk Action"
                                                onChange={(e) =>
                                                    onBulkActionDropdownChange(
                                                        e
                                                    )
                                                }
                                                id="bulk-action"
                                            />
                                        </div>
                                        <div className="btn-wrap">
                                            <span className="d-none d-md-block">
                                                <Button
                                                    disabled={
                                                        actionText === ''
                                                    }
                                                    color="light"
                                                    outline
                                                    className="btn-dim"
                                                    onClick={(e) =>
                                                        onBulkActionButtonClick(
                                                            e
                                                        )
                                                    }
                                                >
                                                    Apply
                                                </Button>
                                            </span>
                                            <span className="d-md-none">
                                                <Button
                                                    color="light"
                                                    outline
                                                    disabled={
                                                        actionText === ''
                                                    }
                                                    className="btn-dim  btn-icon"
                                                    onClick={(e) =>
                                                        onBulkActionButtonClick(
                                                            e
                                                        )
                                                    }
                                                >
                                                    <Icon name="arrow-right"></Icon>
                                                </Button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="card-tools me-n1">
                                    <ul className="btn-toolbar gx-1">
                                        <li>
                                            <a
                                                href="#search"
                                                onClick={(ev) => {
                                                    ev.preventDefault();
                                                    toggleSearchFilter();
                                                }}
                                                className="btn btn-icon search-toggle toggle-search"
                                            >
                                                <Icon name="search"></Icon>
                                            </a>
                                        </li>
                                        <li className="btn-toolbar-sep"></li>
                                        <li>
                                            <div className="toggle-wrap">
                                                <Button
                                                    className={`btn-icon btn-trigger toggle ${
                                                        tablesm ? 'active' : ''
                                                    }`}
                                                    onClick={() =>
                                                        updateTableSm(true)
                                                    }
                                                >
                                                    <Icon name="menu-right"></Icon>
                                                </Button>
                                                <div
                                                    className={`toggle-content ${
                                                        tablesm
                                                            ? 'content-active'
                                                            : ''
                                                    }`}
                                                >
                                                    <ul className="btn-toolbar gx-1">
                                                        <li className="toggle-close">
                                                            <Button
                                                                className="btn-icon btn-trigger toggle"
                                                                onClick={() =>
                                                                    updateTableSm(
                                                                        false
                                                                    )
                                                                }
                                                            >
                                                                <Icon name="arrow-left"></Icon>
                                                            </Button>
                                                        </li>
                                                        <li>
                                                            <UncontrolledDropdown>
                                                                <DropdownToggle
                                                                    tag="a"
                                                                    className="btn btn-trigger btn-icon dropdown-toggle"
                                                                >
                                                                    {appliedFilters
                                                                        ? (
                                                                            <div className="dot dot-primary"></div>
                                                                        )
                                                                        : (
                                                                            ''
                                                                        )}
                                                                    <Icon name="filter-alt"></Icon>
                                                                </DropdownToggle>
                                                                <DropdownMenu
                                                                    end
                                                                    className="filter-wg dropdown-menu-xl"
                                                                    style={{
                                                                        overflow:
                                                                            'visible'
                                                                    }}
                                                                >
                                                                    <div className="dropdown-head">
                                                                        <span className="sub-title dropdown-title">
                                                                            Filter
                                                                            Categories
                                                                        </span>
                                                                    </div>
                                                                    <div className="dropdown-body dropdown-body-rg">
                                                                        <Row className="gx-6 gy-3">
                                                                            <Col
                                                                                sm={
                                                                                    12
                                                                                }
                                                                            >
                                                                                <div className="form-group">
                                                                                    <label className="overline-title overline-title-alt">
                                                                                        Brand
                                                                                        Created
                                                                                        Between
                                                                                    </label>
                                                                                    <div className="form-control-wrap">
                                                                                        <DatePicker
                                                                                            selected={
                                                                                                appliedFilters.start
                                                                                            }
                                                                                            startDate={
                                                                                                appliedFilters.start
                                                                                            }
                                                                                            onChange={
                                                                                                onDateRangeFilterChange
                                                                                            }
                                                                                            endDate={
                                                                                                appliedFilters.end
                                                                                            }
                                                                                            selectsRange
                                                                                            className="form-control date-picker"
                                                                                        />{' '}
                                                                                    </div>
                                                                                    <div className="form-note">
                                                                                        Date
                                                                                        Format{' '}
                                                                                        <code>
                                                                                            mm/dd/yyyy
                                                                                        </code>
                                                                                    </div>
                                                                                </div>
                                                                            </Col>

                                                                            <Col size="12">
                                                                                <div className="form-group">
                                                                                    <label className="overline-title overline-title-alt">
                                                                                        Status
                                                                                    </label>
                                                                                    <RSelect
                                                                                        options={
                                                                                            statuses
                                                                                        }
                                                                                        placeholder="Select"
                                                                                        onChange={(e) => { onStatusFilterChange(e.value); }}
                                                                                        value={appliedFilters.status}
                                                                                    />
                                                                                </div>
                                                                            </Col>
                                                                        </Row>
                                                                    </div>
                                                                    <div className="dropdown-foot between">
                                                                        <a
                                                                            href="#reset"
                                                                            onClick={(
                                                                                ev
                                                                            ) => {
                                                                                ev.preventDefault();
                                                                                resetFilter();
                                                                            }}
                                                                            className="clickable"
                                                                        >
                                                                            Reset Filter
                                                                        </a>
                                                                    </div>
                                                                </DropdownMenu>
                                                            </UncontrolledDropdown>
                                                        </li>
                                                        <li>
                                                            <UncontrolledDropdown>
                                                                <DropdownToggle
                                                                    tag="a"
                                                                    className="btn btn-trigger btn-icon dropdown-toggle"
                                                                >
                                                                    <Icon name="setting"></Icon>
                                                                </DropdownToggle>
                                                                <DropdownMenu
                                                                    end
                                                                    className="dropdown-menu-xs"
                                                                >
                                                                    <ul className="link-check">
                                                                        <li>
                                                                            <span>
                                                                                Show
                                                                            </span>
                                                                        </li>
                                                                        <li
                                                                            className={
                                                                                itemPerPage ===
                                                                                10
                                                                                    ? 'active'
                                                                                    : ''
                                                                            }
                                                                        >
                                                                            <DropdownItem
                                                                                tag="a"
                                                                                href="#dropdownitem"
                                                                                onClick={(
                                                                                    ev
                                                                                ) => {
                                                                                    ev.preventDefault();
                                                                                    setItemPerPage(
                                                                                        10
                                                                                    );
                                                                                }}
                                                                            >
                                                                                10
                                                                            </DropdownItem>
                                                                        </li>
                                                                        <li
                                                                            className={
                                                                                itemPerPage ===
                                                                                15
                                                                                    ? 'active'
                                                                                    : ''
                                                                            }
                                                                        >
                                                                            <DropdownItem
                                                                                tag="a"
                                                                                href="#dropdownitem"
                                                                                onClick={(
                                                                                    ev
                                                                                ) => {
                                                                                    ev.preventDefault();
                                                                                    setItemPerPage(
                                                                                        20
                                                                                    );
                                                                                }}
                                                                            >
                                                                                20
                                                                            </DropdownItem>
                                                                        </li>
                                                                        <li
                                                                            className={
                                                                                itemPerPage ===
                                                                                15
                                                                                    ? 'active'
                                                                                    : ''
                                                                            }
                                                                        >
                                                                            <DropdownItem
                                                                                tag="a"
                                                                                href="#dropdownitem"
                                                                                onClick={(
                                                                                    ev
                                                                                ) => {
                                                                                    ev.preventDefault();
                                                                                    setItemPerPage(
                                                                                        50
                                                                                    );
                                                                                }}
                                                                            >
                                                                                50
                                                                            </DropdownItem>
                                                                        </li>
                                                                    </ul>
                                                                    <ul className="link-check">
                                                                        <li>
                                                                            <span>
                                                                                Order
                                                                            </span>
                                                                        </li>
                                                                        <li
                                                                            className={
                                                                                sort ===
                                                                                'dsc'
                                                                                    ? 'active'
                                                                                    : ''
                                                                            }
                                                                        >
                                                                            <DropdownItem
                                                                                tag="a"
                                                                                href="#dropdownitem"
                                                                                onClick={(
                                                                                    ev
                                                                                ) => {
                                                                                    ev.preventDefault();
                                                                                    setSortState(
                                                                                        'dsc'
                                                                                    );
                                                                                    onSort(
                                                                                        'dsc'
                                                                                    );
                                                                                }}
                                                                            >
                                                                                DESC
                                                                            </DropdownItem>
                                                                        </li>
                                                                        <li
                                                                            className={
                                                                                sort ===
                                                                                'asc'
                                                                                    ? 'active'
                                                                                    : ''
                                                                            }
                                                                        >
                                                                            <DropdownItem
                                                                                tag="a"
                                                                                href="#dropdownitem"
                                                                                onClick={(
                                                                                    ev
                                                                                ) => {
                                                                                    ev.preventDefault();
                                                                                    setSortState(
                                                                                        'asc'
                                                                                    );
                                                                                    onSort(
                                                                                        'asc'
                                                                                    );
                                                                                }}
                                                                            >
                                                                                ASC
                                                                            </DropdownItem>
                                                                        </li>
                                                                    </ul>
                                                                </DropdownMenu>
                                                            </UncontrolledDropdown>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div
                                className={`card-search search-wrap ${
                                    !onSearch && 'active'
                                }`}
                            >
                                <div className="card-body">
                                    <div className="search-content">
                                        <Button
                                            className="search-back btn-icon toggle-search active"
                                            onClick={() => {
                                                setSearchText('');
                                                toggleSearchFilter();
                                            }}
                                        >
                                            <Icon name="arrow-left"></Icon>
                                        </Button>
                                        <input
                                            type="text"
                                            className="border-transparent form-focus-none form-control"
                                            placeholder="Search by category name or status"
                                            value={onSearchText}
                                            onChange={(e) =>
                                                setSearchText(e.target.value)
                                            }
                                        />
                                        <Button className="search-submit btn-icon">
                                            <Icon name="search"></Icon>
                                        </Button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/* END: Table Header */}

                        {/* START: Table Body */}
                        <DataTableBody compact>
                            <DataTableHead>
                                <DataTableRow className="nk-tb-col-check">
                                    <div className="custom-control custom-control-sm custom-checkbox notext">
                                        <input
                                            type="checkbox"
                                            className="custom-control-input"
                                            onChange={(e) =>
                                                onSelectAllChange(e)
                                            }
                                            id="uid"
                                        />
                                        <label
                                            className="custom-control-label"
                                            htmlFor="uid"
                                        ></label>
                                    </div>
                                </DataTableRow>
                                <DataTableRow>
                                    <span className="sub-text">Name</span>
                                </DataTableRow>
                                <DataTableRow>
                                    <span className="sub-text">Status</span>
                                </DataTableRow>
                                <DataTableRow size="lg">
                                    <span className="sub-text">Created At</span>
                                </DataTableRow>
                                <DataTableRow size="lg">
                                    <span className="sub-text">Updated At</span>
                                </DataTableRow>
                                <DataTableRow className="nk-tb-col-tools text-end">
                                    <span className="sub-text">Action</span>
                                </DataTableRow>
                            </DataTableHead>
                            {/* Head */}
                            {currentItems.length > 0
                                ? currentItems.map((item) => {
                                    return (
                                        <DataTableItem key={item._id}>
                                            <DataTableRow className="nk-tb-col-check">
                                                <div className="custom-control custom-control-sm custom-checkbox notext">
                                                    <input
                                                        type="checkbox"
                                                        className="custom-control-input"
                                                        defaultChecked={
                                                            item.checked
                                                        }
                                                        id={item._id + 'uid1'}
                                                        key={Math.random()}
                                                        onChange={(e) =>
                                                            onSelectChange(
                                                                e,
                                                                item._id
                                                            )
                                                        }
                                                    />
                                                    <label
                                                        className="custom-control-label"
                                                        htmlFor={
                                                            item._id + 'uid1'
                                                        }
                                                    ></label>
                                                </div>
                                            </DataTableRow>
                                            <DataTableRow>
                                                <Link
                                                    to={`${process.env.PUBLIC_URL}/account/brands/edit/${item._id}`}
                                                >
                                                    <div className="user-card">
                                                        <div className="user-info">
                                                            <span className="tb-lead">
                                                                {capitalize(
                                                                    item.name
                                                                )}
                                                            </span>
                                                        </div>
                                                    </div>
                                                </Link>
                                            </DataTableRow>
                                            <DataTableRow>
                                                <span
                                                    className={`tb-status text-${
                                                        (item.deletedAt === undefined || item.deletedAt === null || item.deletedAt === '') ? (item.status === 'active' ? 'success' : 'danger') : 'danger'}`}
                                                >
                                                    {capitalize(
                                                        item.deletedAt ===
                                                              undefined ||
                                                              item.deletedAt ===
                                                                  null
                                                            ? item.status
                                                            : 'Deleted'
                                                    )}
                                                </span>
                                            </DataTableRow>
                                            <DataTableRow size="lg">
                                                <span>
                                                    {formatDate(
                                                        item.createdAt
                                                    )}
                                                </span>
                                            </DataTableRow>
                                            <DataTableRow size="lg">
                                                <span>
                                                    {item.updatedAt ===
                                                          undefined ||
                                                      item.updatedAt === null
                                                        ? '--'
                                                        : formatDate(
                                                            item.updatedAt
                                                        )}
                                                </span>
                                            </DataTableRow>
                                            <DataTableRow className="nk-tb-col-tools">
                                                <ul className="nk-tb-actions gx-1">
                                                    <li>
                                                        <UncontrolledDropdown>
                                                            <DropdownToggle
                                                                tag="a"
                                                                className="dropdown-toggle btn btn-icon btn-trigger"
                                                            >
                                                                <Icon name="more-h"></Icon>
                                                            </DropdownToggle>
                                                            <DropdownMenu end>
                                                                <ul className="link-list-opt no-bdr">
                                                                    <li>
                                                                        <DropdownItem
                                                                            tag="a"
                                                                            href=""
                                                                            onClick={(
                                                                                ev
                                                                            ) => {
                                                                                ev.preventDefault();
                                                                                history.push(
                                                                                    `${process.env.PUBLIC_URL}/account/brands/edit/${item._id}`
                                                                                );
                                                                            }}
                                                                        >
                                                                            <Icon name="edit"></Icon>
                                                                            <span>
                                                                                  Edit
                                                                            </span>
                                                                        </DropdownItem>
                                                                        <DropdownItem
                                                                            tag="a"
                                                                            href=""
                                                                            onClick={(
                                                                                e
                                                                            ) =>
                                                                                onDeleteClick(
                                                                                    item._id
                                                                                )
                                                                            }
                                                                        >
                                                                            <Icon name="trash"></Icon>
                                                                            <span>
                                                                                  Delete
                                                                            </span>
                                                                        </DropdownItem>
                                                                    </li>
                                                                </ul>
                                                            </DropdownMenu>
                                                        </UncontrolledDropdown>
                                                    </li>
                                                </ul>
                                            </DataTableRow>
                                        </DataTableItem>
                                    );
                                })
                                : null}
                        </DataTableBody>
                        {/* END: Table Body */}

                        {/* START: Pagination */}
                        <div className="card-inner">
                            {currentItems.length > 0
                                ? (
                                    <PaginationComponent
                                        itemPerPage={itemPerPage}
                                        totalItems={data.length}
                                        paginate={paginate}
                                        currentPage={currentPage}
                                    />
                                )
                                : (
                                    <div className="text-center">
                                        <span className="text-silent">
                                        No data found
                                        </span>
                                    </div>
                                )}
                        </div>
                        {/* END: Pagination */}
                    </DataTable>
                </Block>
            </Content>
            <ToastContainer />
        </React.Fragment>
    );
};

Index.propTypes = {
    pageTitle: PropTypes.string.isRequired
};

export default Index;
