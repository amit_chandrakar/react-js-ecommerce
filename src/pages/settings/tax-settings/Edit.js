import React, { useState } from 'react';
import {
    Icon
} from '../../../components/Component';
import {
    Button,
    Col,
    Row,
    Label,
    Form
} from 'reactstrap';
import axios from 'axios';
import { APP_URL } from '../../../utils/Constants';
import { showToast } from '../../../utils/Utils';
import { ToastContainer } from 'react-toastify';
import { useForm } from 'react-hook-form';
import classNames from 'classnames';
import PropTypes from 'prop-types';

function Edit (props) {
    const { errors, register, handleSubmit } = useForm();
    const [tax] = useState(props.data);
    console.log(props.data);

    const formClass = classNames({
        'form-validate': true,
        'is-alter': true
    });

    const onFormSubmit = async (e) => {
        e.companyId = '5f9d88f9d4b7a1b2c8c7e8b1';

        try {
            const response = await axios.put(`${APP_URL}/taxes/update/${tax._id}`, e);
            showToast('success', response.data.message);
            props.loadData(); // Reload the data
            props.onClose(); // Close the modal
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }
    };

    return (
        <Form className={formClass} onSubmit={handleSubmit(onFormSubmit)}>
            <Row className="g-gs">
                <Col md="4">
                    <div className="form-group">
                        <Label className="form-label" htmlFor="fv-name">
                            Name
                        </Label>
                        <div className="form-control-wrap">
                            <input
                                ref={register({
                                    required: true
                                })}
                                type="text"
                                id="fv-name"
                                name="name"
                                className="form-control"
                                defaultValue={tax.name}
                            />
                            {errors.name && (
                                <span className="invalid">
                                    This field is required
                                </span>
                            )}
                        </div>
                    </div>
                </Col>
                <Col md="4">
                    <div className="form-group">
                        <Label className="form-label" htmlFor="fv-rate">
                            Rate
                        </Label>
                        <div className="form-control-wrap">
                            <input
                                ref={register({
                                    required: true
                                })}
                                type="text"
                                id="fv-rate"
                                name="rate"
                                className="form-control"
                                defaultValue={tax.rate}
                            />
                            {errors.rate && (
                                <span className="invalid">
                                    This field is required
                                </span>
                            )}
                        </div>
                    </div>
                </Col>
                <Col md="4">
                    <div className="form-group">
                        <Label className="form-label" htmlFor="fv-status">
                            Status
                        </Label>
                        <div className="form-control-wrap">
                            <div className="form-control-select">
                                <select
                                    ref={register({
                                        required: true
                                    })}
                                    className="form-control form-select"
                                    id="fv-status"
                                    name="status"
                                    placeholder="Select a option"
                                >
                                    <option
                                        label="Select status"
                                        value=""
                                        selected={tax.status === ''}
                                    ></option>
                                    <option
                                        value="active"
                                        selected={tax.status === 'active'}
                                    >Active</option>
                                    <option
                                        value="inactive"
                                        selected={tax.status === 'inactive'}
                                    >Inactive</option>
                                </select>
                                {errors.status && (
                                    <span className="invalid">
                                        This field is required
                                    </span>
                                )}
                            </div>
                        </div>
                    </div>
                </Col>
                <Col md="12">
                    <div className="form-group">
                        <Button color="primary">
                            <Icon name="check"></Icon>
                            <span>Save</span>
                        </Button>
                    </div>
                </Col>
            </Row>
            <ToastContainer />
        </Form>
    );
}

Edit.propTypes = {
    data: PropTypes.object,
    loadData: PropTypes.func,
    onClose: PropTypes.func
};

export default Edit;
