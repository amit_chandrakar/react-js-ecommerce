import React, { useState } from 'react';
import { Block, Icon } from '../../../components/Component';
import { Button, Col, Form, Label, Row } from 'reactstrap';
import { useForm } from 'react-hook-form';
import classNames from 'classnames';
import axios from 'axios';
import { APP_URL } from '../../../utils/Constants';
import { showToast } from '../../../utils/Utils';
import PropTypes from 'prop-types';

function Google (props) {
    const [tab] = useState(props.tab);
    const { errors, register, handleSubmit } = useForm();
    const [companySettings] = useState(props.companySettings);
    const [status, setStatus] = useState(companySettings.googleStatus === 'active');

    const formClass = classNames({
        'form-validate': true,
        'is-alter': true
    });

    const onFormSubmit = async (e) => {
        e.googleStatus = status ? 'active' : 'inactive';

        try {
            const response = await axios.put(
                `${APP_URL}/social-auth-settings/update`,
                e
            );
            showToast('success', response.data.message);
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }
    };

    return (
        <React.Fragment>
            <div className={tab === '1' ? 'card-inner' : 'd-none'} id="paypal">
                <Block>
                    <Form
                        className={formClass}
                        onSubmit={handleSubmit(onFormSubmit)}
                    >
                        <Row className="gy-4">
                            <Col sm="12" md="12">
                                <div className="preview-block">
                                    <div className="custom-control custom-checkbox">
                                        <input
                                            type="checkbox"
                                            className="custom-control-input"
                                            id="status"
                                            checked={status}
                                            onChange={(e) =>
                                                setStatus(e.target.checked)
                                            }
                                        />
                                        <label
                                            className="custom-control-label form-label"
                                            htmlFor="status"
                                        >
                                        Status
                                        </label>
                                    </div>
                                </div>
                            </Col>
                            <Col md="6" className={ status ? '' : 'd-none'} >
                                <div className="form-group">
                                    <Label
                                        className="form-label"
                                        htmlFor="fv-googleClientId"
                                    >
                                    Google App ID
                                    </Label>
                                    <div className="form-control-wrap">
                                        <input
                                            ref={register({
                                                required: true
                                            })}
                                            type="text"
                                            id="fv-googleClientId"
                                            name="googleClientId"
                                            className="form-control"
                                            defaultValue={companySettings.googleClientId}
                                        />
                                        {errors.googleClientId && (
                                            <span className="invalid">
                                            This field is required
                                            </span>
                                        )}
                                    </div>
                                </div>
                            </Col>
                            <Col md="6" className={ status ? '' : 'd-none'}>
                                <div className="form-group">
                                    <Label
                                        className="form-label"
                                        htmlFor="fv-googleClientSecret"
                                    >
                                    Google Secret
                                    </Label>
                                    <div className="form-control-wrap">
                                        <input
                                            ref={register({
                                                required: true
                                            })}
                                            type="text"
                                            id="fv-googleClientSecret"
                                            name="googleClientSecret"
                                            className="form-control"
                                            defaultValue={companySettings.googleClientSecret}
                                        />
                                        {errors.googleClientSecret && (
                                            <span className="invalid">
                                            This field is required
                                            </span>
                                        )}
                                    </div>
                                </div>
                            </Col>
                            <Col md="12" className={ status ? '' : 'd-none'}>
                                <div className="form-group">
                                    <Label
                                        className="form-label"
                                        htmlFor="fv-company-name"
                                    >
                                    Callback
                                    </Label>
                                    <div className="form-control-wrap">
                                        <span className="form-span">
                                            {APP_URL}/callback/google
                                        </span>{' '}
                                        <a
                                            href="#export"
                                            onClick={(ev) => {
                                                ev.preventDefault();
                                                navigator.clipboard.writeText(`${APP_URL}/callback/google`);
                                                showToast('success', 'Copied to clipboard');
                                            }}
                                            className="btn btn-white btn-outline-light ml-2"
                                        >
                                            <Icon name="copy"></Icon>
                                            <span>Copy</span>
                                        </a>
                                    </div>
                                </div>
                            </Col>
                            <Col md="12">
                                <div className="form-group">
                                    <Button color="primary">
                                        <Icon name="check"></Icon>
                                        <span>Save</span>
                                    </Button>
                                </div>
                            </Col>
                        </Row>
                    </Form>
                </Block>
            </div>
        </React.Fragment>
    );
}

Google.propTypes = {
    tab: PropTypes.string,
    companySettings: PropTypes.object
};

export default Google;
