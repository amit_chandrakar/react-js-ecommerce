import React, { useState } from 'react';
import { Block, Icon } from '../../../components/Component';
import { Button, Col, Form, Label, Row } from 'reactstrap';
import { useForm } from 'react-hook-form';
import classNames from 'classnames';
import axios from 'axios';
import { APP_URL } from '../../../utils/Constants';
import { showToast } from '../../../utils/Utils';
import PropTypes from 'prop-types';

function Facebook (props) {
    const [tab] = useState(props.tab);
    const { errors, register, handleSubmit } = useForm();
    const [companySettings] = useState(props.companySettings);
    const [status, setStatus] = useState(companySettings.facebookStatus === 'active');

    const formClass = classNames({
        'form-validate': true,
        'is-alter': true
    });

    const onFormSubmit = async (e) => {
        e.facebookStatus = status ? 'active' : 'inactive';

        try {
            const response = await axios.put(
                `${APP_URL}/social-auth-settings/update`,
                e
            );
            showToast('success', response.data.message);
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }
    };

    return (
        <React.Fragment>
            <div className={tab === '2' ? 'card-inner' : 'd-none'} id="paypal">
                <Block>
                    <Form
                        className={formClass}
                        onSubmit={handleSubmit(onFormSubmit)}
                    >
                        <Row className="gy-4">
                            <Col sm="12" md="12">
                                <div className="preview-block">
                                    <div className="custom-control custom-checkbox">
                                        <input
                                            type="checkbox"
                                            className="custom-control-input"
                                            id="status"
                                            checked={status}
                                            onChange={(e) =>
                                                setStatus(e.target.checked)
                                            }
                                        />
                                        <label
                                            className="custom-control-label form-label"
                                            htmlFor="status"
                                        >
                                        Status
                                        </label>
                                    </div>
                                </div>
                            </Col>
                            <Col md="6" className={ status ? '' : 'd-none'} >
                                <div className="form-group">
                                    <Label
                                        className="form-label"
                                        htmlFor="fv-facebookClientId"
                                    >
                                    Facebook App ID
                                    </Label>
                                    <div className="form-control-wrap">
                                        <input
                                            ref={register({
                                                required: true
                                            })}
                                            type="text"
                                            id="fv-facebookClientId"
                                            name="facebookClientId"
                                            className="form-control"
                                            defaultValue={companySettings.facebookClientId}
                                        />
                                        {errors.facebookClientId && (
                                            <span className="invalid">
                                            This field is required
                                            </span>
                                        )}
                                    </div>
                                </div>
                            </Col>
                            <Col md="6" className={ status ? '' : 'd-none'}>
                                <div className="form-group">
                                    <Label
                                        className="form-label"
                                        htmlFor="fv-facebookClientSecret"
                                    >
                                    Facebook Secret
                                    </Label>
                                    <div className="form-control-wrap">
                                        <input
                                            ref={register({
                                                required: true
                                            })}
                                            type="text"
                                            id="fv-facebookClientSecret"
                                            name="facebookClientSecret"
                                            className="form-control"
                                            defaultValue={companySettings.facebookClientSecret}
                                        />
                                        {errors.facebookClientSecret && (
                                            <span className="invalid">
                                            This field is required
                                            </span>
                                        )}
                                    </div>
                                </div>
                            </Col>
                            <Col md="12" className={ status ? '' : 'd-none'}>
                                <div className="form-group">
                                    <Label
                                        className="form-label"
                                        htmlFor="fv-company-name"
                                    >
                                    Callback
                                    </Label>
                                    <div className="form-control-wrap">
                                        <span className="form-span">
                                            {APP_URL}/callback/facebook
                                        </span>{' '}
                                        <a
                                            href="#export"
                                            onClick={(ev) => {
                                                ev.preventDefault();
                                                navigator.clipboard.writeText(`${APP_URL}/callback/facebook`);
                                                showToast('success', 'Copied to clipboard');
                                            }}
                                            className="btn btn-white btn-outline-light ml-2"
                                        >
                                            <Icon name="copy"></Icon>
                                            <span>Copy</span>
                                        </a>
                                    </div>
                                </div>
                            </Col>
                            <Col md="12">
                                <div className="form-group">
                                    <Button color="primary">
                                        <Icon name="check"></Icon>
                                        <span>Save</span>
                                    </Button>
                                </div>
                            </Col>
                        </Row>
                    </Form>
                </Block>
            </div>
        </React.Fragment>
    );
}

Facebook.propTypes = {
    tab: PropTypes.string,
    companySettings: PropTypes.object
};

export default Facebook;
