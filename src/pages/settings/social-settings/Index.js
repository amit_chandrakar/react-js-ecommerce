import React, { useEffect, useState } from 'react';
import { Icon } from '../../../components/Component';
import { APP_URL } from '../../../utils/Constants';
import { showToast } from '../../../utils/Utils';
import axios from 'axios';
import { ToastContainer } from 'react-toastify';
import Google from './Google';
import Facebook from './Facebook';
import Linkedin from './Linkedin';
import Twitter from './Twitter';
import Head from '../../../layout/head/Head';
import PropTypes from 'prop-types';

function Index ({ pageTitle }) {
    const [tab, setTab] = useState('0');
    const [companySettings, setCompanySettings] = useState([]);

    // Get all the languages
    const getCompanySettings = async () => {
        await axios
            .get(`${APP_URL}/social-auth-settings/show`)
            .then((response) => {
                setCompanySettings(response.data.data.socialAuthSetting);
                setTab('1');
            })
            .catch((error) => {
                showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
            });
    };

    useEffect(() => {
        getCompanySettings();
    }, []);

    return (
        <React.Fragment>
            <Head title={pageTitle}></Head>
            <div className="card-aside-wrap" id="user-detail-block">
                <div className="card-content">
                    <ul className="nav nav-tabs nav-tabs-mb-icon nav-tabs-card">
                        <li className="nav-item">
                            <a
                                className={
                                    tab === '1' ? 'nav-link active' : 'nav-link'
                                }
                                href="#paypal"
                                onClick={(ev) => {
                                    ev.preventDefault();
                                    setTab('1');
                                }}
                            >
                                <Icon name="google"></Icon>
                                <span className="me-2">Google</span>
                                <span
                                    className={`dot dot-lg dot-${
                                        companySettings.googleStatus === 'active'
                                            ? 'success'
                                            : 'danger'
                                    }`}
                                ></span>
                            </a>
                        </li>
                        <li className="nav-item">
                            <a
                                className={
                                    tab === '2' ? 'nav-link active' : 'nav-link'
                                }
                                href="#stripe"
                                onClick={(ev) => {
                                    ev.preventDefault();
                                    setTab('2');
                                }}
                            >
                                <Icon name="facebook-fill"></Icon>
                                <span className="me-2">Facebook</span>
                                <span
                                    className={`dot dot-lg dot-${
                                        companySettings.facebookStatus === 'active'
                                            ? 'success'
                                            : 'danger'
                                    }`}
                                ></span>
                            </a>
                        </li>
                        <li className="nav-item">
                            <a
                                className={
                                    tab === '3' ? 'nav-link active' : 'nav-link'
                                }
                                href="#razorpay"
                                onClick={(ev) => {
                                    ev.preventDefault();
                                    setTab('3');
                                }}
                            >
                                <Icon name="linkedin"></Icon>
                                <span className="me-2">LinkedIn</span>
                                <span
                                    className={`dot dot-lg dot-${
                                        companySettings.linkedinStatus === 'active'
                                            ? 'success'
                                            : 'danger'
                                    }`}
                                ></span>
                            </a>
                        </li>
                        <li className="nav-item">
                            <a
                                className={
                                    tab === '4' ? 'nav-link active' : 'nav-link'
                                }
                                href="#razorpay"
                                onClick={(ev) => {
                                    ev.preventDefault();
                                    setTab('4');
                                }}
                            >
                                <Icon name="twitter"></Icon>
                                <span className="me-2">Twitter</span>
                                <span
                                    className={`dot dot-lg dot-${
                                        companySettings.twitterStatus === 'active'
                                            ? 'success'
                                            : 'danger'
                                    }`}
                                ></span>
                            </a>
                        </li>
                    </ul>

                    {tab === '1'
                        ? (
                            <Google tab={tab} companySettings={companySettings} />
                        )
                        : tab === '2'
                            ? (
                                <Facebook tab={tab} companySettings={companySettings} />
                            )
                            : tab === '3'
                                ? (
                                    <Linkedin tab={tab} companySettings={companySettings} />
                                )
                                : tab === '4'
                                    ? (
                                        <Twitter tab={tab} companySettings={companySettings} />
                                    )
                                    : null}

                </div>
            </div>
            <ToastContainer />
        </React.Fragment>
    );
}

Index.propTypes = {
    pageTitle: PropTypes.string.isRequired
};

export default Index;
