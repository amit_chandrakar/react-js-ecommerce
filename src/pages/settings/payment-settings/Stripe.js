import React, { useEffect, useState } from 'react';
import {
    Block,
    Icon,
    Row,
    Col,
    Button
} from '../../../components/Component';
import { useForm } from 'react-hook-form';
import classNames from 'classnames';
import { Label, Form } from 'reactstrap';
import { APP_URL, defaultPaymentEnvironments } from '../../../utils/Constants';
import { showToast } from '../../../utils/Utils';
import axios from 'axios';
import PropTypes from 'prop-types';

function Stripe (props) {
    const [tab, setTab] = useState('');
    const { errors, register, handleSubmit } = useForm();
    const [companySettings, setCompanySettings] = useState({});
    const [paymentGatewayStatus, setPaymentGatewayStatus] = useState({});
    const [paymentGatewayEnvironment, setPaymentGatewayEnvironment] = useState(
        {}
    );
    const formClass = classNames({
        'form-validate': true,
        'is-alter': true
    });

    useEffect(() => {
        setTab(props.tab);
        setCompanySettings(props.companySettings);
        setPaymentGatewayStatus(props.paymentGatewayStatus);
        setPaymentGatewayEnvironment(props.paymentGatewayEnvironment);
        console.log(props);
    }, []);

    const onFormSubmit = async (e) => {
        // console.log(e);
        try {
            const response = await axios.put(
                `${APP_URL}/company-settings/update`,
                e
            );
            showToast('success', response.data.message);
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }
    };

    return (
        <React.Fragment>
            {/* START: Stripe setting tab */}
            <div className={tab === '2' ? 'card-inner' : 'd-none'} id="stripe">
                <Block>
                    <Form
                        className={formClass}
                        onSubmit={handleSubmit(onFormSubmit)}
                    >
                        <Row className="gy-4">
                            <Col sm="12" md="12">
                                <div className="preview-block">
                                    <div className="custom-control custom-checkbox">
                                        <input
                                            type="checkbox"
                                            className="custom-control-input"
                                            id="stripeStatus"
                                            name="stripeStatus"
                                            checked={
                                                paymentGatewayStatus.stripe
                                            }
                                            onChange={(e) => {
                                                setPaymentGatewayStatus({
                                                    ...paymentGatewayStatus,
                                                    stripe: e.target.checked
                                                });
                                            }}
                                            ref={register({
                                                required: true
                                            })}
                                        />
                                        <label
                                            className="custom-control-label form-label"
                                            htmlFor="stripeStatus"
                                        >
                                            Stripe Status{' '}
                                            {paymentGatewayStatus.stripe}
                                        </label>
                                    </div>
                                </div>
                            </Col>
                            <Col
                                sm="12"
                                md="12"
                                className={
                                    paymentGatewayStatus.stripe ? '' : 'd-none'
                                }
                            >
                                <div className="form-group">
                                    <label className="form-label">
                                        Select Environment
                                    </label>
                                    <select
                                        ref={register({
                                            required: true
                                        })}
                                        className="form-select"
                                        name="stripeEnvironment"
                                        onChange={(e) => {
                                            setPaymentGatewayEnvironment({
                                                ...paymentGatewayEnvironment,
                                                stripe: e.target.value
                                            });
                                        }}
                                    >
                                        <option value="">Select</option>
                                        {defaultPaymentEnvironments.map(
                                            (item, index) => {
                                                return (
                                                    <option
                                                        key={index}
                                                        value={item.value}
                                                        selected={
                                                            paymentGatewayEnvironment.stripe ===
                                                            item.value
                                                        }
                                                    >
                                                        {item.label}
                                                    </option>
                                                );
                                            }
                                        )}
                                    </select>
                                    {errors.stripeEnvironment && (
                                        <span className="invalid">
                                            This field is required
                                        </span>
                                    )}
                                </div>
                            </Col>
                            <Col
                                md="6"
                                className={
                                    paymentGatewayStatus.stripe &&
                                    paymentGatewayEnvironment.stripe ===
                                        'sandbox'
                                        ? ''
                                        : 'd-none'
                                }
                            >
                                <div className="form-group">
                                    <Label
                                        className="form-label"
                                        htmlFor="fv-stripeSandboxPublishableKey"
                                    >
                                        Sandbox Stripe Client ID
                                    </Label>
                                    <div className="form-control-wrap">
                                        <input
                                            ref={register({
                                                required: true
                                            })}
                                            type="text"
                                            id="fv-stripeSandboxPublishableKey"
                                            name="stripeSandboxPublishableKey"
                                            className="form-control"
                                            defaultValue={
                                                companySettings.stripeSandboxPublishableKey
                                            }
                                        />
                                        {errors.stripeSandboxPublishableKey && (
                                            <span className="invalid">
                                                This field is required
                                            </span>
                                        )}
                                    </div>
                                </div>
                            </Col>
                            <Col
                                md="6"
                                className={
                                    paymentGatewayStatus.stripe &&
                                    paymentGatewayEnvironment.stripe ===
                                        'sandbox'
                                        ? ''
                                        : 'd-none'
                                }
                            >
                                <div className="form-group">
                                    <Label
                                        className="form-label"
                                        htmlFor="fv-stripeSandboxSecretKey"
                                    >
                                        Sandbox Stripe Secret
                                    </Label>
                                    <div className="form-control-wrap">
                                        <input
                                            ref={register({
                                                required: true
                                            })}
                                            type="text"
                                            id="fv-stripeSandboxSecretKey"
                                            name="stripeSandboxSecretKey"
                                            className="form-control"
                                            defaultValue={
                                                companySettings.stripeSandboxSecretKey
                                            }
                                        />
                                        {errors.stripeSandboxSecretKey && (
                                            <span className="invalid">
                                                This field is required
                                            </span>
                                        )}
                                    </div>
                                </div>
                            </Col>
                            <Col
                                md="6"
                                className={
                                    paymentGatewayStatus.stripe &&
                                    paymentGatewayEnvironment.stripe === 'live'
                                        ? ''
                                        : 'd-none'
                                }
                            >
                                <div className="form-group">
                                    <Label
                                        className="form-label"
                                        htmlFor="fv-stripeLivePublishableKey"
                                    >
                                        Live Stripe Client ID
                                    </Label>
                                    <div className="form-control-wrap">
                                        <input
                                            ref={register({
                                                required: true
                                            })}
                                            type="text"
                                            id="fv-stripeLivePublishableKey"
                                            name="stripeLivePublishableKey"
                                            className="form-control"
                                            defaultValue={
                                                companySettings.stripeLivePublishableKey
                                            }
                                        />
                                        {errors.stripeLivePublishableKey && (
                                            <span className="invalid">
                                                This field is required
                                            </span>
                                        )}
                                    </div>
                                </div>
                            </Col>
                            <Col
                                md="6"
                                className={
                                    paymentGatewayStatus.stripe &&
                                    paymentGatewayEnvironment.stripe === 'live'
                                        ? ''
                                        : 'd-none'
                                }
                            >
                                <div className="form-group">
                                    <Label
                                        className="form-label"
                                        htmlFor="fv-stripeLiveSecretKey"
                                    >
                                        Live Stripe Secret
                                    </Label>
                                    <div className="form-control-wrap">
                                        <input
                                            ref={register({
                                                required: true
                                            })}
                                            type="text"
                                            id="fv-stripeLiveSecretKey"
                                            name="stripeLiveSecretKey"
                                            className="form-control"
                                            defaultValue={
                                                companySettings.stripeLiveSecretKey
                                            }
                                        />
                                        {errors.stripeLiveSecretKey && (
                                            <span className="invalid">
                                                This field is required
                                            </span>
                                        )}
                                    </div>
                                </div>
                            </Col>
                            <Col md="12">
                                <div className="form-group">
                                    <Button color="primary">
                                        <Icon name="check"></Icon>
                                        <span>Save</span>
                                    </Button>
                                </div>
                            </Col>
                        </Row>
                    </Form>
                </Block>
            </div>
            {/* END: Stripe setting tab */}
        </React.Fragment>
    );
}

Stripe.propTypes = {
    tab: PropTypes.string,
    companySettings: PropTypes.object,
    paymentGatewayStatus: PropTypes.object,
    paymentGatewayEnvironment: PropTypes.object
};

export default Stripe;
