import React, { useEffect, useState } from 'react';
import {
    Block,
    Icon,
    Row,
    Col,
    Button
} from '../../../components/Component';
import { useForm } from 'react-hook-form';
import classNames from 'classnames';
import { Label, Form } from 'reactstrap';
import { APP_URL, defaultPaymentEnvironments } from '../../../utils/Constants';
import { showToast } from '../../../utils/Utils';
import axios from 'axios';
import PropTypes from 'prop-types';

function Paypal (props) {
    const [tab, setTab] = useState('');
    const { errors, register, handleSubmit } = useForm();
    const [companySettings, setCompanySettings] = useState({});
    const [paymentGatewayStatus, setPaymentGatewayStatus] = useState({});
    const [paymentGatewayEnvironment, setPaymentGatewayEnvironment] = useState({});
    const formClass = classNames({
        'form-validate': true,
        'is-alter': true
    });

    useEffect(() => {
        setTab(props.tab);
        setCompanySettings(props.companySettings);
        setPaymentGatewayStatus(props.paymentGatewayStatus);
        setPaymentGatewayEnvironment(props.paymentGatewayEnvironment);
    }, []);

    const onFormSubmit = async (e) => {
        try {
            const response = await axios.put(
                `${APP_URL}/company-settings/update`,
                e
            );
            showToast('success', response.data.message);
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }
    };

    return (
        <React.Fragment>
            {/* START: Paypal setting tab */}
            <div className={tab === '1' ? 'card-inner' : 'd-none'} id="paypal">
                <Block>
                    <Form
                        className={formClass}
                        onSubmit={handleSubmit(onFormSubmit)}
                    >
                        <Row className="gy-4">
                            <Col sm="12" md="12">
                                <div className="preview-block">
                                    <div className="custom-control custom-checkbox">
                                        <input
                                            type="checkbox"
                                            className="custom-control-input"
                                            id="paypalStatus"
                                            name="paypalStatus"
                                            checked={ paymentGatewayStatus.paypal }
                                            onChange={(e) => {
                                                setPaymentGatewayStatus({
                                                    ...paymentGatewayStatus,
                                                    paypal: e.target.checked
                                                });
                                            }}
                                            ref={register({
                                                required: true
                                            })}
                                        />
                                        <label
                                            className="custom-control-label form-label"
                                            htmlFor="paypalStatus"
                                        >
                                            Paypal Status {paymentGatewayStatus.paypal}
                                        </label>
                                    </div>
                                </div>
                            </Col>
                            <Col
                                sm="12"
                                md="12"
                                className={
                                    paymentGatewayStatus.paypal ? '' : 'd-none'
                                }
                            >
                                <div className="form-group">
                                    <label className="form-label">
                                        Select Environment
                                    </label>
                                    <select
                                        ref={register({
                                            required: true
                                        })}
                                        className="form-select"
                                        name="paypalEnvironment"
                                        onChange={(e) => {
                                            setPaymentGatewayEnvironment({
                                                ...paymentGatewayEnvironment,
                                                paypal: e.target.value
                                            });
                                        }}
                                    >
                                        <option value="">Select</option>
                                        {defaultPaymentEnvironments.map(
                                            (item, index) => {
                                                return (
                                                    <option
                                                        key={index}
                                                        value={item.value}
                                                        selected={
                                                            paymentGatewayEnvironment.paypal ===
                                                            item.value
                                                        }
                                                    >
                                                        {item.label}
                                                    </option>
                                                );
                                            }
                                        )}
                                    </select>
                                    {errors.paypalEnvironment && (
                                        <span className="invalid">
                                            This field is required
                                        </span>
                                    )}
                                </div>
                            </Col>
                            <Col
                                md="6"
                                className={
                                    paymentGatewayStatus.paypal &&
                                    paymentGatewayEnvironment.paypal ===
                                        'sandbox'
                                        ? ''
                                        : 'd-none'
                                }
                            >
                                <div className="form-group">
                                    <Label
                                        className="form-label"
                                        htmlFor="fv-paypalSandboxClientId"
                                    >
                                        Sandbox Paypal Client ID
                                    </Label>
                                    <div className="form-control-wrap">
                                        <input
                                            ref={register({
                                                required: true
                                            })}
                                            type="text"
                                            id="fv-paypalSandboxClientId"
                                            name="paypalSandboxClientId"
                                            className="form-control"
                                            defaultValue={
                                                companySettings.paypalSandboxClientId
                                            }
                                        />
                                        {errors.paypalSandboxClientId && (
                                            <span className="invalid">
                                                This field is required
                                            </span>
                                        )}
                                    </div>
                                </div>
                            </Col>
                            <Col
                                md="6"
                                className={
                                    paymentGatewayStatus.paypal &&
                                    paymentGatewayEnvironment.paypal ===
                                        'sandbox'
                                        ? ''
                                        : 'd-none'
                                }
                            >
                                <div className="form-group">
                                    <Label
                                        className="form-label"
                                        htmlFor="fv-paypalSandboxClientSecret"
                                    >
                                        Sandbox Paypal Secret
                                    </Label>
                                    <div className="form-control-wrap">
                                        <input
                                            ref={register({
                                                required: true
                                            })}
                                            type="text"
                                            id="fv-paypalSandboxClientSecret"
                                            name="paypalSandboxClientSecret"
                                            className="form-control"
                                            defaultValue={
                                                companySettings.paypalSandboxClientSecret
                                            }
                                        />
                                        {errors.paypalSandboxClientSecret && (
                                            <span className="invalid">
                                                This field is required
                                            </span>
                                        )}
                                    </div>
                                </div>
                            </Col>
                            <Col
                                md="6"
                                className={
                                    paymentGatewayStatus.paypal &&
                                    paymentGatewayEnvironment.paypal === 'live'
                                        ? ''
                                        : 'd-none'
                                }
                            >
                                <div className="form-group">
                                    <Label
                                        className="form-label"
                                        htmlFor="fv-paypalLiveClientId"
                                    >
                                        Live Paypal Client ID
                                    </Label>
                                    <div className="form-control-wrap">
                                        <input
                                            ref={register({
                                                required: true
                                            })}
                                            type="text"
                                            id="fv-paypalLiveClientId"
                                            name="paypalLiveClientId"
                                            className="form-control"
                                            defaultValue={
                                                companySettings.paypalLiveClientId
                                            }
                                        />
                                        {errors.paypalLiveClientId && (
                                            <span className="invalid">
                                                This field is required
                                            </span>
                                        )}
                                    </div>
                                </div>
                            </Col>
                            <Col
                                md="6"
                                className={
                                    paymentGatewayStatus.paypal &&
                                    paymentGatewayEnvironment.paypal === 'live'
                                        ? ''
                                        : 'd-none'
                                }
                            >
                                <div className="form-group">
                                    <Label
                                        className="form-label"
                                        htmlFor="fv-paypalLiveClientSecret"
                                    >
                                        Live Paypal Secret
                                    </Label>
                                    <div className="form-control-wrap">
                                        <input
                                            ref={register({
                                                required: true
                                            })}
                                            type="text"
                                            id="fv-paypalLiveClientSecret"
                                            name="paypalLiveClientSecret"
                                            className="form-control"
                                            defaultValue={
                                                companySettings.paypalLiveClientSecret
                                            }
                                        />
                                        {errors.paypalLiveClientSecret && (
                                            <span className="invalid">
                                                This field is required
                                            </span>
                                        )}
                                    </div>
                                </div>
                            </Col>
                            <Col md="12">
                                <div className="form-group">
                                    <Button color="primary">
                                        <Icon name="check"></Icon>
                                        <span>Save</span>
                                    </Button>
                                </div>
                            </Col>
                        </Row>
                    </Form>
                </Block>
            </div>
            {/* END: Paypal setting tab */}
        </React.Fragment>
    );
}

Paypal.propTypes = {
    tab: PropTypes.string,
    companySettings: PropTypes.object,
    paymentGatewayStatus: PropTypes.object,
    paymentGatewayEnvironment: PropTypes.object
};

export default Paypal;
