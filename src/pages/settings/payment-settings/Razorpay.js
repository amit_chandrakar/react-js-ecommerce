import React, { useEffect, useState } from 'react';
import {
    Block,
    Icon,
    Row,
    Col,
    Button
} from '../../../components/Component';
import { useForm } from 'react-hook-form';
import classNames from 'classnames';
import { Label, Form } from 'reactstrap';
import { APP_URL, defaultPaymentEnvironments } from '../../../utils/Constants';
import { showToast } from '../../../utils/Utils';
import axios from 'axios';
import PropTypes from 'prop-types';

function Razorpay (props) {
    const [tab, setTab] = useState('');
    const { errors, register, handleSubmit } = useForm();
    const [companySettings, setCompanySettings] = useState({});
    const [paymentGatewayStatus, setPaymentGatewayStatus] = useState({});
    const [paymentGatewayEnvironment, setPaymentGatewayEnvironment] = useState(
        {}
    );
    const formClass = classNames({
        'form-validate': true,
        'is-alter': true
    });

    useEffect(() => {
        setTab(props.tab);
        setCompanySettings(props.companySettings);
        setPaymentGatewayStatus(props.paymentGatewayStatus);
        setPaymentGatewayEnvironment(props.paymentGatewayEnvironment);
        console.log(props);
    }, []);

    const onFormSubmit = async (e) => {
        try {
            const response = await axios.put(
                `${APP_URL}/company-settings/update`,
                e
            );
            showToast('success', response.data.message);
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }
    };

    return (
        <React.Fragment>
            {/* START: Razorpay tab */}
            <div
                className={tab === '3' ? 'card-inner' : 'd-none'}
                id="razorpay"
            >
                <Block>
                    <Form
                        className={formClass}
                        onSubmit={handleSubmit(onFormSubmit)}
                    >
                        <Row className="gy-4">
                            <Col sm="12" md="12">
                                <div className="preview-block">
                                    <div className="custom-control custom-checkbox">
                                        <input
                                            type="checkbox"
                                            className="custom-control-input"
                                            id="razorpayStatus"
                                            name="razorpayStatus"
                                            checked={
                                                paymentGatewayStatus.razorpay
                                            }
                                            onChange={(e) => {
                                                setPaymentGatewayStatus({
                                                    ...paymentGatewayStatus,
                                                    razorpay: e.target.checked
                                                });
                                            }}
                                            ref={register({
                                                required: true
                                            })}
                                        />
                                        <label
                                            className="custom-control-label form-label"
                                            htmlFor="razorpayStatus"
                                        >
                                            Razorpay Status{' '}
                                            {paymentGatewayStatus.razorpay}
                                        </label>
                                    </div>
                                </div>
                            </Col>
                            <Col
                                sm="12"
                                md="12"
                                className={
                                    paymentGatewayStatus.razorpay
                                        ? ''
                                        : 'd-none'
                                }
                            >
                                <div className="form-group">
                                    <label className="form-label">
                                        Select Environment
                                    </label>
                                    <select
                                        ref={register({
                                            required: true
                                        })}
                                        className="form-select"
                                        name="razorpayEnvironment"
                                        onChange={(e) => {
                                            setPaymentGatewayEnvironment({
                                                ...paymentGatewayEnvironment,
                                                razorpay: e.target.value
                                            });
                                        }}
                                    >
                                        <option value="">Select</option>
                                        {defaultPaymentEnvironments.map(
                                            (item, index) => {
                                                return (
                                                    <option
                                                        key={index}
                                                        value={item.value}
                                                        selected={
                                                            paymentGatewayEnvironment.razorpay ===
                                                            item.value
                                                        }
                                                    >
                                                        {item.label}
                                                    </option>
                                                );
                                            }
                                        )}
                                    </select>
                                    {errors.razorpayEnvironment && (
                                        <span className="invalid">
                                            This field is required
                                        </span>
                                    )}
                                </div>
                            </Col>
                            <Col
                                md="6"
                                className={
                                    paymentGatewayStatus.razorpay &&
                                    paymentGatewayEnvironment.razorpay ===
                                        'sandbox'
                                        ? ''
                                        : 'd-none'
                                }
                            >
                                <div className="form-group">
                                    <Label
                                        className="form-label"
                                        htmlFor="fv-razorpaySandboxKeyId"
                                    >
                                        Sandbox Razorpay Client ID
                                    </Label>
                                    <div className="form-control-wrap">
                                        <input
                                            ref={register({
                                                required: true
                                            })}
                                            type="text"
                                            id="fv-razorpaySandboxKeyId"
                                            name="razorpaySandboxKeyId"
                                            className="form-control"
                                            defaultValue={
                                                companySettings.razorpaySandboxKeyId
                                            }
                                        />
                                        {errors.razorpaySandboxKeyId && (
                                            <span className="invalid">
                                                This field is required
                                            </span>
                                        )}
                                    </div>
                                </div>
                            </Col>
                            <Col
                                md="6"
                                className={
                                    paymentGatewayStatus.razorpay &&
                                    paymentGatewayEnvironment.razorpay ===
                                        'sandbox'
                                        ? ''
                                        : 'd-none'
                                }
                            >
                                <div className="form-group">
                                    <Label
                                        className="form-label"
                                        htmlFor="fv-razorpaySandboxKeySecret"
                                    >
                                        Sandbox Razorpay Secret
                                    </Label>
                                    <div className="form-control-wrap">
                                        <input
                                            ref={register({
                                                required: true
                                            })}
                                            type="text"
                                            id="fv-razorpaySandboxKeySecret"
                                            name="razorpaySandboxKeySecret"
                                            className="form-control"
                                            defaultValue={
                                                companySettings.razorpaySandboxKeySecret
                                            }
                                        />
                                        {errors.razorpaySandboxKeySecret && (
                                            <span className="invalid">
                                                This field is required
                                            </span>
                                        )}
                                    </div>
                                </div>
                            </Col>
                            <Col
                                md="6"
                                className={
                                    paymentGatewayStatus.razorpay &&
                                    paymentGatewayEnvironment.razorpay ===
                                        'live'
                                        ? ''
                                        : 'd-none'
                                }
                            >
                                <div className="form-group">
                                    <Label
                                        className="form-label"
                                        htmlFor="fv-razorpayLiveKeyId"
                                    >
                                        Live Razorpay Client ID
                                    </Label>
                                    <div className="form-control-wrap">
                                        <input
                                            ref={register({
                                                required: true
                                            })}
                                            type="text"
                                            id="fv-razorpayLiveKeyId"
                                            name="razorpayLiveKeyId"
                                            className="form-control"
                                            defaultValue={
                                                companySettings.razorpayLiveKeyId
                                            }
                                        />
                                        {errors.razorpayLiveKeyId && (
                                            <span className="invalid">
                                                This field is required
                                            </span>
                                        )}
                                    </div>
                                </div>
                            </Col>
                            <Col
                                md="6"
                                className={
                                    paymentGatewayStatus.razorpay &&
                                    paymentGatewayEnvironment.razorpay ===
                                        'live'
                                        ? ''
                                        : 'd-none'
                                }
                            >
                                <div className="form-group">
                                    <Label
                                        className="form-label"
                                        htmlFor="fv-razorpayLiveKeySecret"
                                    >
                                        Live Razorpay Secret
                                    </Label>
                                    <div className="form-control-wrap">
                                        <input
                                            ref={register({
                                                required: true
                                            })}
                                            type="text"
                                            id="fv-razorpayLiveKeySecret"
                                            name="razorpayLiveKeySecret"
                                            className="form-control"
                                            defaultValue={
                                                companySettings.razorpayLiveKeySecret
                                            }
                                        />
                                        {errors.razorpayLiveKeySecret && (
                                            <span className="invalid">
                                                This field is required
                                            </span>
                                        )}
                                    </div>
                                </div>
                            </Col>
                            <Col md="12">
                                <div className="form-group">
                                    <Button color="primary">
                                        <Icon name="check"></Icon>
                                        <span>Save</span>
                                    </Button>
                                </div>
                            </Col>
                        </Row>
                    </Form>
                </Block>
            </div>
            {/* END: Razorpay tab */}
        </React.Fragment>
    );
}

Razorpay.propTypes = {
    tab: PropTypes.string,
    companySettings: PropTypes.object,
    paymentGatewayStatus: PropTypes.object,
    paymentGatewayEnvironment: PropTypes.object
};

export default Razorpay;
