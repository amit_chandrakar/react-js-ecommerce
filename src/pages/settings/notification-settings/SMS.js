import React, { useState } from 'react';
import {
    Block,
    Icon,
    Row,
    Col,
    Button
} from '../../../components/Component';
import { useForm } from 'react-hook-form';
import classNames from 'classnames';
import { Label, Form } from 'reactstrap';
import axios from 'axios';
import { APP_URL } from '../../../utils/Constants';
import { showToast } from '../../../utils/Utils';
import PropTypes from 'prop-types';

function SMS (props) {
    const [tab] = useState(props.tab);
    const { errors, register, handleSubmit } = useForm();
    const [settings] = useState(props.settings.sms);
    const [status, setStatus] = useState(
        props.settings.sms.status === 'active'
    );

    const formClass = classNames({
        'form-validate': true,
        'is-alter': true
    });

    const onFormSubmit = async (e) => {
        e.status = status ? 'active' : 'inactive';

        try {
            const response = await axios.put(
                `${APP_URL}/sms-settings/update`,
                e
            );
            showToast('success', response.data.message);
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }
    };

    return (
        <React.Fragment>
            <div className={tab === '2' ? 'card-inner' : 'd-none'} id="sms">
                <Block>
                    <Form
                        className={formClass}
                        onSubmit={handleSubmit(onFormSubmit)}
                    >
                        <Row className="gy-4">
                            <Col sm="12" md="12">
                                <div className="preview-block">
                                    <div className="custom-control custom-checkbox">
                                        <input
                                            type="checkbox"
                                            className="custom-control-input"
                                            id="customCheck1"
                                            name="status"
                                            checked={status}
                                            onChange={(e) =>
                                                setStatus(e.target.checked)
                                            }
                                        />
                                        <label
                                            className="custom-control-label form-label"
                                            htmlFor="customCheck1"
                                        >
                                            Status
                                        </label>
                                    </div>
                                </div>
                            </Col>

                            <Col md="6" className={ status ? '' : 'd-none'}>
                                <div className="form-group">
                                    <Label
                                        className="form-label"
                                        htmlFor="fv-sid"
                                    >
                                        SMS SID
                                    </Label>
                                    <div className="form-control-wrap">
                                        <input
                                            ref={register({
                                                required: true
                                            })}
                                            type="text"
                                            id="fv-sid"
                                            className="form-control"
                                            name="smsSid"
                                            defaultValue={settings.smsSid}
                                        />
                                        {errors.smsSid && (
                                            <span className="invalid">
                                                This field is required
                                            </span>
                                        )}
                                    </div>
                                </div>
                            </Col>
                            <Col md="6" className={ status ? '' : 'd-none'}>
                                <div className="form-group">
                                    <Label
                                        className="form-label"
                                        htmlFor="fv-token"
                                    >
                                        SMS Auth Token
                                    </Label>
                                    <div className="form-control-wrap">
                                        <input
                                            ref={register({
                                                required: true
                                            })}
                                            type="text"
                                            id="fv-token"
                                            className="form-control"
                                            name="smsAuthToken"
                                            defaultValue={settings.smsAuthToken}
                                        />
                                        {errors.smsAuthToken && (
                                            <span className="invalid">
                                                This field is required
                                            </span>
                                        )}
                                    </div>
                                </div>
                            </Col>
                            <Col md="6" className={ status ? '' : 'd-none'}>
                                <div className="form-group">
                                    <Label
                                        className="form-label"
                                        htmlFor="fv-smsFromNumber"
                                    >
                                        Mail From Number
                                    </Label>
                                    <div className="form-control-wrap">
                                        <input
                                            ref={register({
                                                required: true
                                            })}
                                            type="text"
                                            id="fv-smsFromNumber"
                                            className="form-control"
                                            name="smsFromNumber"
                                            defaultValue={settings.smsFromNumber}
                                        />
                                        {errors.smsFromNumber && (
                                            <span className="invalid">
                                                This field is required
                                            </span>
                                        )}
                                    </div>
                                </div>
                            </Col>
                            <Col md="12">
                                <div className="form-group">
                                    <Button color="primary">
                                        <Icon name="check"></Icon>
                                        <span>Save</span>
                                    </Button>
                                </div>
                            </Col>
                        </Row>
                    </Form>
                </Block>
            </div>
        </React.Fragment>
    );
}

SMS.propTypes = {
    tab: PropTypes.string,
    settings: PropTypes.object
};

export default SMS;
