import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { APP_URL } from '../../../utils/Constants';
import { showToast } from '../../../utils/Utils';
import EmailSettings from './Email';
import { ToastContainer } from 'react-toastify';
import PushNotification from './PushNotification';
import SMS from './SMS';
import Head from '../../../layout/head/Head';
import PropTypes from 'prop-types';

function Index ({ pageTitle }) {
    const [tab, setTab] = useState('0');
    const [settings, setSettings] = useState({
        email: {},
        sms: {},
        pushNotification: {}
    });

    const [notificationStatus, setNotificationStatus] = useState({
        email: false,
        sms: false,
        pushNotification: false
    });

    // Get all the languages
    const getSettings = async () => {
        await axios
            .get(`${APP_URL}/email-settings/show`)
            .then((response) => {
                setSettings((prevEnvironment) => ({
                    ...prevEnvironment,
                    email: response.data.data.emailSetting
                }));
                setNotificationStatus((prevEnvironment) => ({
                    ...prevEnvironment,
                    email: response.data.data.emailSetting.status === 'active'
                }));
            })
            .catch((error) => {
                showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
            });

        await axios
            .get(`${APP_URL}/sms-settings/show`)
            .then((response) => {
                setSettings((prevEnvironment) => ({
                    ...prevEnvironment,
                    sms: response.data.data.smsSetting
                }));
                setNotificationStatus((prevEnvironment) => ({
                    ...prevEnvironment,
                    sms: response.data.data.smsSetting.status === 'active'
                }));
            })
            .catch((error) => {
                showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
            });

        await axios
            .get(`${APP_URL}/push-notification-settings/show`)
            .then((response) => {
                setSettings((prevEnvironment) => ({
                    ...prevEnvironment,
                    pushNotification:
                        response.data.data.pushNotificationSetting
                }));
                setNotificationStatus((prevEnvironment) => ({
                    ...prevEnvironment,
                    pushNotification: response.data.data.pushNotificationSetting.status === 'active'
                }));
            })
            .catch((error) => {
                showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
            });

        setTab('1');
    };

    useEffect(() => {
        getSettings();
    }, []);

    return (
        <React.Fragment>
            <Head title={pageTitle}></Head>
            <div className="card-aside-wrap" id="user-detail-block">
                <div className="card-content">
                    <ul className="nav nav-tabs nav-tabs-mb-icon nav-tabs-card">
                        <li className="nav-item">
                            <a
                                className={
                                    tab === '1' ? 'nav-link active' : 'nav-link'
                                }
                                href="#paypal"
                                onClick={(ev) => {
                                    ev.preventDefault();
                                    setTab('1');
                                }}
                            >
                                <span className="me-2">Email</span>
                                <span
                                    className={`dot dot-lg dot-${
                                        notificationStatus.email
                                            ? 'success'
                                            : 'danger'
                                    }`}
                                ></span>
                            </a>
                        </li>
                        <li className="nav-item">
                            <a
                                className={
                                    tab === '2' ? 'nav-link active' : 'nav-link'
                                }
                                href="#stripee"
                                onClick={(ev) => {
                                    ev.preventDefault();
                                    setTab('2');
                                }}
                            >
                                <span className="me-2">SMS</span>
                                <span
                                    className={`dot dot-lg dot-${
                                        notificationStatus.sms
                                            ? 'success'
                                            : 'danger'
                                    }`}
                                ></span>
                            </a>
                        </li>
                        <li className="nav-item">
                            <a
                                className={
                                    tab === '3' ? 'nav-link active' : 'nav-link'
                                }
                                href="#razorpayy"
                                onClick={(ev) => {
                                    ev.preventDefault();
                                    setTab('3');
                                }}
                            >
                                <span className="me-2">Push Notification</span>
                                <span
                                    className={`dot dot-lg dot-${
                                        notificationStatus.pushNotification
                                            ? 'success'
                                            : 'danger'
                                    }`}
                                ></span>
                            </a>
                        </li>
                    </ul>

                    {
                        tab === '1'
                            ? (
                                <EmailSettings
                                    tab={tab}
                                    settings={settings}
                                    notificationStatus={notificationStatus}
                                />
                            )
                            : tab === '2'
                                ? (
                                    <SMS
                                        tab={tab}
                                        settings={settings}
                                        notificationStatus={notificationStatus}
                                    />
                                )
                                : tab === '3'
                                    ? (
                                        <PushNotification
                                            tab={tab}
                                            settings={settings}
                                            notificationStatus={notificationStatus}
                                        />
                                    )
                                    : null
                    }
                </div>
            </div>
            <ToastContainer />
        </React.Fragment>
    );
}

Index.propTypes = {
    pageTitle: PropTypes.string.isRequired
};

export default Index;
