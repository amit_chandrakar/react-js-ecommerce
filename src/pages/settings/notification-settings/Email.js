import React, { useState } from 'react';
import {
    Block,
    Icon,
    Row,
    Col,
    Button
} from '../../../components/Component';
import { useForm } from 'react-hook-form';
import classNames from 'classnames';
import { Label, Form } from 'reactstrap';
import axios from 'axios';
import { APP_URL } from '../../../utils/Constants';
import { showToast } from '../../../utils/Utils';
import PropTypes from 'prop-types';

function Email (props) {
    const [tab] = useState(props.tab);
    const { errors, register, handleSubmit } = useForm();
    const [settings] = useState(props.settings.email);
    const [status, setStatus] = useState(props.settings.email.status === 'active');

    const formClass = classNames({
        'form-validate': true,
        'is-alter': true
    });

    const mailDrivers = [
        { value: 'mail', label: 'Mail' },
        { value: 'smtp', label: 'SMTP' }
    ];

    const mailEncryption = [
        { value: 'ssl', label: 'ssl' },
        { value: 'tls', label: 'tls' },
        { value: 'none', label: 'none' }
    ];

    const onFormSubmit = async (e) => {
        e.status = status ? 'active' : 'inactive';

        try {
            const response = await axios.put(`${APP_URL}/email-settings/update`, e);
            showToast('success', response.data.message);
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }
    };

    return (
        <React.Fragment>
            <div className={tab === '1' ? 'card-inner' : 'd-none'} id="email">
                <Block>
                    <Form
                        className={formClass}
                        onSubmit={handleSubmit(onFormSubmit)}
                    >
                        <Row className="gy-4">
                            <Col md="12">
                                <div className="preview-block">
                                    <div className="custom-control custom-checkbox">
                                        <input
                                            type="checkbox"
                                            className="custom-control-input"
                                            id="status"
                                            onChange={ () => setStatus(!status) }
                                            name="status"
                                            checked={ status }
                                            defaultValue={ status ? 'active' : 'inactive'}
                                        />
                                        <label
                                            className="custom-control-label form-label"
                                            htmlFor="status"
                                        >
                                            Email Status
                                        </label>
                                    </div>
                                </div>
                            </Col>
                            <Col md="6" className={ status ? '' : 'd-none'}>
                                <div className="form-group">
                                    <Label
                                        className="form-label"
                                        htmlFor="fv-mail-from-name"
                                    >
                                        Mail From Name
                                    </Label>
                                    <div className="form-control-wrap">
                                        <input
                                            ref={register({
                                                required: true
                                            })}
                                            type="text"
                                            id="fv-mail"
                                            className="form-control"
                                            name="mailFromName"
                                            defaultValue={settings.mailFromName}
                                        />
                                        {errors.mailFromName && (
                                            <span className="invalid">
                                                This field is required
                                            </span>
                                        )}
                                    </div>
                                </div>
                            </Col>
                            <Col md="6" className={ status ? '' : 'd-none'}>
                                <div className="form-group">
                                    <Label
                                        className="form-label"
                                        htmlFor="fv-mail-from-email"
                                    >
                                        Mail From Address
                                    </Label>
                                    <div className="form-control-wrap">
                                        <input
                                            ref={register({
                                                required: true
                                            })}
                                            type="text"
                                            id="fv-mail-from-email"
                                            className="form-control"
                                            name="mailFromAddress"
                                            defaultValue={settings.mailFromAddress}
                                        />
                                        {errors.mailFromAddress && (
                                            <span className="invalid">
                                                This field is required
                                            </span>
                                        )}
                                    </div>
                                </div>
                            </Col>
                            <Col md="12" className={ status ? '' : 'd-none'}>
                                <div className="form-group">
                                    <label className="form-label">
                                        Mail Driver
                                    </label>
                                    <select
                                        ref={register({
                                            required: true
                                        })}
                                        className="form-select form-control"
                                        name="driver"
                                        defaultValue={settings.driver}
                                    >
                                        {mailDrivers.map((item, index) => {
                                            return (
                                                <option
                                                    key={index}
                                                    value={item.value}
                                                >
                                                    {item.label}
                                                </option>
                                            );
                                        }
                                        )}
                                    </select>
                                </div>
                            </Col>
                            <Col md="12" className={ status ? '' : 'd-none'}>
                                <div className="form-group">
                                    <label className="form-label">
                                        Mail Encryption
                                    </label>
                                    <select
                                        ref={register({
                                            required: true
                                        })}
                                        className="form-select form-control"
                                        name="encryption"
                                        defaultValue={settings.encryption}
                                    >
                                        {mailEncryption.map((item, index) => {
                                            return (
                                                <option
                                                    key={index}
                                                    value={item.value}
                                                >
                                                    {item.label}
                                                </option>
                                            );
                                        }
                                        )}
                                    </select>
                                </div>
                            </Col>
                            <Col md="6" className={ status ? '' : 'd-none'}>
                                <div className="form-group">
                                    <Label
                                        className="form-label"
                                        htmlFor="fv-host"
                                    >
                                        Mail Host
                                    </Label>
                                    <div className="form-control-wrap">
                                        <input
                                            ref={register({
                                                required: true
                                            })}
                                            type="text"
                                            id="fv-host"
                                            className="form-control"
                                            name="host"
                                            defaultValue={settings.host}
                                        />
                                        {errors.host && (
                                            <span className="invalid">
                                                This field is required
                                            </span>
                                        )}
                                    </div>
                                </div>
                            </Col>
                            <Col md="6" className={ status ? '' : 'd-none'}>
                                <div className="form-group">
                                    <Label
                                        className="form-label"
                                        htmlFor="fv-port"
                                    >
                                        Mail Port
                                    </Label>
                                    <div className="form-control-wrap">
                                        <input
                                            ref={register({
                                                required: true
                                            })}
                                            type="text"
                                            id="fv-port"
                                            className="form-control"
                                            name="port"
                                            defaultValue={settings.port}
                                        />
                                        {errors.port && (
                                            <span className="invalid">
                                                This field is required
                                            </span>
                                        )}
                                    </div>
                                </div>
                            </Col>
                            <Col md="6" className={ status ? '' : 'd-none'}>
                                <div className="form-group">
                                    <Label
                                        className="form-label"
                                        htmlFor="fv-username"
                                    >
                                        Mail Username
                                    </Label>
                                    <div className="form-control-wrap">
                                        <input
                                            ref={register({
                                                required: true
                                            })}
                                            type="text"
                                            id="fv-username"
                                            className="form-control"
                                            name="username"
                                            defaultValue={settings.username}
                                        />
                                        {errors.username && (
                                            <span className="invalid">
                                                This field is required
                                            </span>
                                        )}
                                    </div>
                                </div>
                            </Col>
                            <Col md="6" className={ status ? '' : 'd-none'}>
                                <div className="form-group">
                                    <Label
                                        className="form-label"
                                        htmlFor="fv-password"
                                    >
                                        Mail Password
                                    </Label>
                                    <div className="form-control-wrap">
                                        <input
                                            ref={register({
                                                required: true
                                            })}
                                            type="text"
                                            id="fv-password"
                                            className="form-control"
                                            name="password"
                                            defaultValue={settings.password}
                                        />
                                        {errors.password && (
                                            <span className="invalid">
                                                This field is required
                                            </span>
                                        )}
                                    </div>
                                </div>
                            </Col>
                            <Col md="12">
                                <div className="form-group">
                                    <Button color="primary me-2">
                                        <Icon name="check"></Icon>
                                        <span>Save</span>
                                    </Button>

                                    <a
                                        href="#export"
                                        onClick={(ev) => {
                                            ev.preventDefault();
                                        }}
                                        className={ status ? 'btn btn-white btn-outline-light ml-2' : 'd-none'}
                                    >
                                        <Icon name="send"></Icon>
                                        <span>Send Test Email</span>
                                    </a>
                                </div>
                            </Col>
                        </Row>
                    </Form>
                </Block>
            </div>
        </React.Fragment>
    );
}

Email.propTypes = {
    tab: PropTypes.string,
    settings: PropTypes.object
};

export default Email;
