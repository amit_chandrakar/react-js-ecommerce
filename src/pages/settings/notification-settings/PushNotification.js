import React, { useState } from 'react';
import {
    Block,
    Icon,
    Row,
    Col,
    Button
} from '../../../components/Component';
import { useForm } from 'react-hook-form';
import classNames from 'classnames';
import { Label, Form } from 'reactstrap';
import axios from 'axios';
import { APP_URL } from '../../../utils/Constants';
import { showToast } from '../../../utils/Utils';
import PropTypes from 'prop-types';

function PushNotification (props) {
    const [tab] = useState(props.tab);
    const { errors, register, handleSubmit } = useForm();
    const [settings] = useState(props.settings.pushNotification);
    const [status, setStatus] = useState(
        props.settings.pushNotification.status === 'active'
    );

    const formClass = classNames({
        'form-validate': true,
        'is-alter': true
    });

    const onFormSubmit = async (e) => {
        e.status = status ? 'active' : 'inactive';

        try {
            const response = await axios.put(
                `${APP_URL}/push-notification-settings/update`,
                e
            );
            showToast('success', response.data.message);
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }
    };

    return (
        <React.Fragment>
            <div className={tab === '3' ? 'card-inner' : 'd-none'} id="email">
                <Block>
                    <Form
                        className={formClass}
                        onSubmit={handleSubmit(onFormSubmit)}
                    >
                        <Row className="gy-4">
                            <Col sm="12" md="12">
                                <div className="preview-block">
                                    <div className="custom-control custom-checkbox">
                                        <input
                                            type="checkbox"
                                            className="custom-control-input"
                                            id="customCheck1"
                                            checked={status}
                                            onChange={(e) =>
                                                setStatus(e.target.checked)
                                            }
                                        />
                                        <label
                                            className="custom-control-label form-label"
                                            htmlFor="customCheck1"
                                        >
                                            Status
                                        </label>
                                    </div>
                                </div>
                            </Col>
                            <Col md="6" className={ status ? '' : 'd-none'}>
                                <div className="form-group">
                                    <Label
                                        className="form-label"
                                        htmlFor="fv-onesignalAppId"
                                    >
                                        One Signal App ID
                                    </Label>
                                    <div className="form-control-wrap">
                                        <input
                                            ref={register({
                                                required: true
                                            })}
                                            type="text"
                                            id="fv-onesignalAppId"
                                            className="form-control"
                                            name="onesignalAppId"
                                            defaultValue={settings.onesignalAppId}
                                        />
                                        {errors.onesignalAppId && (
                                            <span className="invalid">
                                                This field is required
                                            </span>
                                        )}
                                    </div>
                                </div>
                            </Col>
                            <Col md="6" className={ status ? '' : 'd-none'}>
                                <div className="form-group">
                                    <Label
                                        className="form-label"
                                        htmlFor="fv-onesignalApiKey"
                                    >
                                        One Signal Rest API Key
                                    </Label>
                                    <div className="form-control-wrap">
                                        <input
                                            ref={register({
                                                required: true
                                            })}
                                            type="text"
                                            id="fv-onesignalApiKey"
                                            className="form-control"
                                            name="onesignalApiKey"
                                            defaultValue={settings.onesignalApiKey}
                                        />
                                        {errors.onesignalApiKey && (
                                            <span className="invalid">
                                                This field is required
                                            </span>
                                        )}
                                    </div>
                                </div>
                            </Col>
                            <Col md="12">
                                <div className="form-group">
                                    <Button color="primary">
                                        <Icon name="check"></Icon>
                                        <span>Save</span>
                                    </Button>
                                </div>
                            </Col>
                        </Row>
                    </Form>
                </Block>
            </div>
        </React.Fragment>
    );
}

PushNotification.propTypes = {
    settings: PropTypes.object,
    tab: PropTypes.string
};

export default PushNotification;
