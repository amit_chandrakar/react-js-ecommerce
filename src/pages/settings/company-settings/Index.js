import React, { useState, useEffect } from 'react';
import {
    Block,
    BlockBetween,
    BlockDes,
    BlockHead,
    BlockHeadContent,
    BlockTitle,
    Icon,
    Row,
    Col,
    Button
} from '../../../components/Component';
import { useForm } from 'react-hook-form';
import classNames from 'classnames';
import { Label, Form } from 'reactstrap';
import axios from 'axios';
import { ToastContainer } from 'react-toastify';
import { APP_URL } from '../../../utils/Constants';
import { showToast } from '../../../utils/Utils';
import Head from '../../../layout/head/Head';
import PropTypes from 'prop-types';

function Index ({ pageTitle }) {
    const [sm, updateSm] = useState(false);
    const { errors, register, handleSubmit } = useForm();
    const formClass = classNames({
        'form-validate': true,
        'is-alter': true
    });

    const [data, setData] = useState([]);

    const getCompanyDetails = async () => {
        await axios
            .get(`${APP_URL}/companies/show`)
            .then((response) => {
                setData(response.data.data);
            })
            .catch((error) => {
                showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
            });
    };

    useEffect(() => {
        getCompanyDetails();
    }, []);

    const onFormSubmit = async (e) => {
        try {
            const response = await axios.put(`${APP_URL}/companies/update`, e);
            showToast('success', response.data.message);
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }
    };

    return (
        <React.Fragment>
            <Head title={pageTitle}></Head>
            <BlockHead size="lg">
                <BlockBetween>
                    <BlockHeadContent>
                        <BlockTitle tag="h4">Company Settings</BlockTitle>
                        <BlockDes>
                            <p>
                                Basic info, like company name, email, phone, and
                                contact
                            </p>
                        </BlockDes>
                    </BlockHeadContent>
                    <BlockHeadContent className="align-self-start d-lg-none">
                        <Button
                            className={`toggle btn btn-icon btn-trigger mt-n1 ${
                                sm ? 'active' : ''
                            }`}
                            onClick={() => updateSm(!sm)}
                        >
                            <Icon name="menu-alt-r"></Icon>
                        </Button>
                    </BlockHeadContent>
                </BlockBetween>
            </BlockHead>

            <Block size="sm">
                <Form
                    className={formClass}
                    onSubmit={handleSubmit(onFormSubmit)}
                >
                    <Row className="g-gs">
                        <Col md="6">
                            <div className="form-group">
                                <Label
                                    className="form-label"
                                    htmlFor="fv-name"
                                >
                                    Company Name
                                </Label>
                                <div className="form-control-wrap">
                                    <input
                                        ref={register({
                                            required: true
                                        })}
                                        type="text"
                                        id="fv-name"
                                        name="name"
                                        className="form-control"
                                        placeholder="e.g. Apple Inc."
                                        defaultValue={data.name}
                                    />
                                    {errors.companyName && (
                                        <span className="invalid">
                                            Company name is required
                                        </span>
                                    )}
                                </div>
                            </div>
                        </Col>
                        <Col md="6">
                            <div className="form-group">
                                <Label
                                    className="form-label"
                                    htmlFor="fv-email"
                                >
                                    Company Email
                                </Label>
                                <div className="form-control-wrap">
                                    <input
                                        ref={register({
                                            required: true,
                                            pattern: {
                                                value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                                                message:
                                                    'Invalid email address'
                                            }
                                        })}
                                        type="email"
                                        id="fv-email"
                                        name="email"
                                        className="form-control"
                                        placeholder="e.g. admin@example.com"
                                        defaultValue={data.email}
                                    />
                                    {errors.email &&
                                        errors.email.type === 'required' && (
                                        <span className="invalid">
                                                Company email is required
                                        </span>
                                    )}
                                    {errors.email &&
                                        errors.email.type === 'pattern' && (
                                        <span className="invalid">
                                            {errors.email.message}
                                        </span>
                                    )}
                                </div>
                            </div>
                        </Col>
                        <Col md="6">
                            <div className="form-group">
                                <Label
                                    className="form-label"
                                    htmlFor="fv-phone"
                                >
                                    Company Phone
                                </Label>
                                <div className="form-control-wrap">
                                    <input
                                        ref={register({
                                            required: true
                                        })}
                                        type="text"
                                        id="fv-phone"
                                        name="phone"
                                        className="form-control"
                                        placeholder="e.g. +1 (609) 933-44-22"
                                        defaultValue={data.phone}
                                    />
                                    {errors.phone && (
                                        <span className="invalid">
                                            Company phone is required
                                        </span>
                                    )}
                                </div>
                            </div>
                        </Col>
                        <Col md="6">
                            <div className="form-group">
                                <Label
                                    className="form-label"
                                    htmlFor="fv-website"
                                >
                                    Company Website
                                </Label>
                                <div className="form-control-wrap">
                                    <input
                                        ref={register({
                                            required: true
                                        })}
                                        type="text"
                                        id="fv-website"
                                        name="website"
                                        className="form-control"
                                        placeholder="e.g. https://example.com"
                                        defaultValue={data.website}
                                    />
                                    {errors.website && (
                                        <span className="invalid">
                                            This field is required
                                        </span>
                                    )}
                                </div>
                            </div>
                        </Col>
                        <Col md="12">
                            <div className="form-group">
                                <Label
                                    className="form-label"
                                    htmlFor="fv-address"
                                >
                                    Company Address
                                </Label>
                                <div className="form-control-wrap">
                                    <textarea
                                        ref={register({
                                            required: true
                                        })}
                                        type="text"
                                        id="fv-address"
                                        name="address"
                                        className="form-control"
                                        placeholder="e.g. Near Statue of Liberty, New York, USA"
                                        defaultValue={data.address}
                                    />
                                    {errors.address && (
                                        <span className="invalid">
                                            This field is required
                                        </span>
                                    )}
                                </div>
                            </div>
                        </Col>
                        <Col md="12">
                            <div className="form-group">
                                <Button color="primary">
                                    <Icon name="plus"></Icon>
                                    <span>Save</span>
                                </Button>
                            </div>
                        </Col>
                    </Row>
                </Form>
            </Block>
            <ToastContainer />
        </React.Fragment>
    );
}

Index.propTypes = {
    pageTitle: PropTypes.string.isRequired
};

export default Index;
