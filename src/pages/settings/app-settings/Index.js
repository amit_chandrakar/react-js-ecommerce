import React, { useState, useEffect } from 'react';
import {
    Block,
    BlockBetween,
    BlockDes,
    BlockHead,
    BlockHeadContent,
    BlockTitle,
    Icon,
    Row,
    Col,
    Button
} from '../../../components/Component';
import { useForm } from 'react-hook-form';
import classNames from 'classnames';
import { Label, Form } from 'reactstrap';
import axios from 'axios';
import { ToastContainer } from 'react-toastify';
import {
    APP_URL,
    dateFormats,
    timeFormats
} from '../../../utils/Constants';
import {
    showToast
} from '../../../utils/Utils';
import moment from 'moment-timezone';
import Head from '../../../layout/head/Head';
import PropTypes from 'prop-types';

function Index ({ pageTitle }) {
    const { errors, register, handleSubmit } = useForm();
    const formClass = classNames({
        'form-validate': true,
        'is-alter': true
    });

    const [data, setData] = useState([]);
    const [languages, setLanguages] = useState([]);
    const allTimeZones = moment.tz.names();

    const getCompanyDetails = async () => {
        await axios
            .get(`${APP_URL}/companies/show`)
            .then((response) => {
                setData(response.data.data);
                console.log(response.data.data);
            })
            .catch((error) => {
                showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
            });
    };

    // Get all the languages
    const getLanguages = async () => {
        await axios
            .get(`${APP_URL}/languages`)
            .then((response) => {
                setLanguages(response.data.languages);
            })
            .catch((error) => {
                showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
            });
    };

    useEffect(() => {
        getCompanyDetails();
        getLanguages();
    }, []);

    const onFormSubmit = async (e) => {
        try {
            const response = await axios.put(`${APP_URL}/companies/update`, e);
            showToast('success', response.data.message);
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }
    };

    return (
        <React.Fragment>
            <Head title={pageTitle}></Head>
            <BlockHead size="lg">
                <BlockBetween>
                    <BlockHeadContent>
                        <BlockTitle tag="h4">App Settings</BlockTitle>
                        <BlockDes>
                            <p>
                                Basic info, like time zone, language, date
                                format, and time format
                            </p>
                        </BlockDes>
                    </BlockHeadContent>
                </BlockBetween>
            </BlockHead>

            <Block size="sm">
                <Form
                    className={formClass}
                    onSubmit={handleSubmit(onFormSubmit)}
                >
                    <Row className="g-gs">
                        <Col md="3">
                            <div className="form-group">
                                <Label
                                    className="form-label"
                                    htmlFor="fv-dateFormat"
                                >
                                    Date Format
                                </Label>
                                <div className="form-control-wrap">
                                    <div className="form-control-select">
                                        <select
                                            ref={register({
                                                required: true
                                            })}
                                            className="form-control form-select"
                                            id="fv-dateFormat"
                                            name="dateFormat"
                                            placeholder="Select a option"
                                        >
                                            {dateFormats.map((item, index) => (
                                                <option
                                                    key={index}
                                                    value={item.value}
                                                    selected={
                                                        data.dateFormat ===
                                                        item.value
                                                    }
                                                >
                                                    {item.label}
                                                </option>
                                            ))}
                                        </select>
                                        {errors.dateFormat && (
                                            <span className="invalid">
                                                This field is required
                                            </span>
                                        )}
                                    </div>
                                </div>
                            </div>
                        </Col>
                        <Col md="3">
                            <div className="form-group">
                                <Label
                                    className="form-label"
                                    htmlFor="fv-timeFormat"
                                >
                                    Time Format
                                </Label>
                                <div className="form-control-wrap">
                                    <div className="form-control-select">
                                        <select
                                            ref={register({
                                                required: true
                                            })}
                                            className="form-control form-select"
                                            id="fv-timeFormat"
                                            name="timeFormat"
                                            placeholder="Select a option"
                                        >
                                            {timeFormats.map((item, index) => (
                                                <option
                                                    key={index}
                                                    value={item.value}
                                                    selected={
                                                        data.timeFormat ===
                                                        item.value
                                                    }
                                                >
                                                    {item.label}
                                                </option>
                                            ))}
                                        </select>
                                        {errors.timeFormat && (
                                            <span className="invalid">
                                                This field is required
                                            </span>
                                        )}
                                    </div>
                                </div>
                            </div>
                        </Col>
                        <Col md="3">
                            <div className="form-group">
                                <Label
                                    className="form-label"
                                    htmlFor="fv-timeZone"
                                >
                                    Default Time Zone
                                </Label>
                                <div className="form-control-wrap">
                                    <div className="form-control-select">
                                        <select
                                            ref={register({
                                                required: true
                                            })}
                                            className="form-control form-select"
                                            id="fv-timeZone"
                                            name="timeZone"
                                            placeholder="Select a option"
                                        >
                                            {allTimeZones.map((item) => (
                                                <option
                                                    key={item}
                                                    value={item}
                                                    selected={
                                                        data.timeZone === item
                                                    }
                                                >
                                                    {item}
                                                </option>
                                            ))}
                                        </select>
                                        {errors.timeZone && (
                                            <span className="invalid">
                                                This field is required
                                            </span>
                                        )}
                                    </div>
                                </div>
                            </div>
                        </Col>
                        <Col md="3">
                            <div className="form-group">
                                <Label
                                    className="form-label"
                                    htmlFor="fv-language"
                                >
                                    Default Language
                                </Label>
                                <div className="form-control-wrap">
                                    <div className="form-control-select">
                                        <select
                                            ref={register({
                                                required: true
                                            })}
                                            className="form-control form-select"
                                            id="fv-language"
                                            name="language"
                                            placeholder="Select a option"
                                        >
                                            {languages.map((item, index) => (
                                                <option
                                                    key={index}
                                                    value={item._id}
                                                    selected={
                                                        data.language ===
                                                        item._id
                                                    }
                                                >
                                                    {item.name}
                                                </option>
                                            ))}
                                        </select>
                                        {errors.language && (
                                            <span className="invalid">
                                                This field is required
                                            </span>
                                        )}
                                    </div>
                                </div>
                            </div>
                        </Col>
                        <Col md="12">
                            <div className="form-group">
                                <Button color="primary">
                                    <Icon name="check"></Icon>
                                    <span>Save</span>
                                </Button>
                            </div>
                        </Col>
                    </Row>
                </Form>
            </Block>
            <ToastContainer />
        </React.Fragment>
    );
}

Index.propTypes = {
    pageTitle: PropTypes.string.isRequired
};

export default Index;
