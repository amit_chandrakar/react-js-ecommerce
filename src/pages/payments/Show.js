import React, { useState, useEffect } from 'react';
import Content from '../../layout/content/Content';
import Head from '../../layout/head/Head';
import LogoDark from '../../images/logo.png';
import {
    BlockHead,
    Button,
    Icon,
    BlockDes,
    BlockHeadContent,
    Block,
    BlockBetween
} from '../../components/Component';
import { Link } from 'react-router-dom';
import { useParams } from 'react-router';
import axios from 'axios';
import { showToast } from '../../utils/Utils';
import { APP_URL, formatDate } from '../../utils/Constants';
import PropTypes from 'prop-types';
import { useTitle } from '../../contexts/TitleContext';

const Show = ({ match, pageTitle }) => {
    const [order, setOrder] = useState();

    const { id } = useParams();

    const { setDocumentTitle } = useTitle();
    useEffect(() => {
        setDocumentTitle(pageTitle);
    }, [pageTitle, setDocumentTitle]);

    const getOrder = async () => {
        let response;

        try {
            response = await axios.get(`${APP_URL}/orders/edit/${id}`);
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }

        setOrder(response.data.data.order);
    };

    useEffect(() => {
        // Fetching data from api
        getOrder();
    }, []);

    return (
        <React.Fragment>
            <Head title={pageTitle}></Head>
            {order && (
                <Content>
                    <BlockHead>
                        <BlockBetween className="g-3">
                            <BlockHeadContent>
                                <BlockDes className="text-soft">
                                    <ul className="list-inline">
                                        <li>
                                            Order Placed At:{' '}
                                            <span className="text-base">
                                                {formatDate(order.createdAt)}
                                            </span>
                                        </li>
                                        <li>
                                            Order Updated At:{' '}
                                            <span className="text-base">
                                                {formatDate(order.createdAt)}
                                            </span>
                                        </li>
                                        <li>
                                            Status:{' '}
                                            <span
                                                className={`tb-status h6 text-${
                                                    order.status === 'cancelled'
                                                        ? 'danger'
                                                        : 'success'
                                                }`}
                                            >
                                                {order.status}
                                            </span>
                                        </li>
                                    </ul>
                                </BlockDes>
                            </BlockHeadContent>
                            <BlockHeadContent>
                                <Link
                                    to={`${process.env.PUBLIC_URL}/account/orders`}
                                >
                                    <Button
                                        color="light"
                                        outline
                                        className="bg-white d-none d-sm-inline-flex me-2"
                                    >
                                        <Icon name="edit"></Icon>
                                        <span>Edit</span>
                                    </Button>
                                </Link>
                                <Link
                                    to={`${process.env.PUBLIC_URL}/account/orders`}
                                >
                                    <Button
                                        color="light"
                                        outline
                                        className="bg-white d-none d-sm-inline-flex me-2"
                                    >
                                        <Icon name="download"></Icon>
                                        <span>Download</span>
                                    </Button>
                                </Link>
                                <Link
                                    to={`${process.env.PUBLIC_URL}/account/orders`}
                                >
                                    <Button
                                        color="light"
                                        outline
                                        className="bg-white d-none d-sm-inline-flex"
                                    >
                                        <Icon name="arrow-left"></Icon>
                                        <span>Back</span>
                                    </Button>
                                </Link>
                                <Link
                                    to={`${process.env.PUBLIC_URL}/account/orders`}
                                >
                                    <Button
                                        color="light"
                                        outline
                                        className="btn-icon bg-white d-inline-flex d-sm-none"
                                    >
                                        <Icon name="arrow-left"></Icon>
                                    </Button>
                                </Link>
                            </BlockHeadContent>
                        </BlockBetween>
                    </BlockHead>

                    <Block>
                        <div className="invoice">
                            <div className="invoice-wrap">
                                <div className="invoice-brand text-center">
                                    <img src={LogoDark} alt="" />
                                </div>

                                <div className="invoice-head">
                                    <div className="invoice-contact">
                                        <span className="overline-title">
                                            Orders To
                                        </span>
                                        <div className="invoice-contact-info">
                                            <h4 className="title">
                                                {order.user.name}
                                            </h4>
                                            <ul className="list-plain">
                                                <li>
                                                    <Icon name="map-pin-fill"></Icon>
                                                    <span>
                                                        {
                                                            order.address
                                                                .houseNumber
                                                        }{' '}
                                                        {order.address.address}{' '}
                                                        {order.address.landmark}
                                                        <br />
                                                        {order.address.city}
                                                        {', '}
                                                        {
                                                            order.address.state
                                                        }{' '}
                                                        {order.address.pincode}{' '}
                                                    </span>
                                                </li>
                                                <li>
                                                    <Icon name="call-fill"></Icon>
                                                    <span>
                                                        {order.user.mobile}
                                                    </span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div className="invoice-desc">
                                        <h3 className="title">Orders</h3>
                                        <ul className="list-plain">
                                            <li className="invoice-id">
                                                <span>Orders ID</span>:
                                                <span>746F5K2</span>
                                            </li>
                                            <li className="invoice-date">
                                                <span>Date</span>:
                                                <span>
                                                    {formatDate(
                                                        order.createdAt
                                                    )}
                                                </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <div className="invoice-bills">
                                    <div className="table-responsive">
                                        <table className="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th className="w-150px">
                                                        Item ID
                                                    </th>
                                                    <th className="w-60">
                                                        Description
                                                    </th>
                                                    <th>Price</th>
                                                    <th>Qty</th>
                                                    <th>Amount</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>24108054</td>
                                                    <td>
                                                        Conceptual
                                                        App Dashboard - Regular
                                                        License
                                                    </td>
                                                    <td>$100.00</td>
                                                    <td>1</td>
                                                    <td>$100.00</td>
                                                </tr>
                                                <tr>
                                                    <td>24108054</td>
                                                    <td>
                                                        24 months premium
                                                        support
                                                    </td>
                                                    <td>$200.00</td>
                                                    <td>1</td>
                                                    <td>$200.00</td>
                                                </tr>
                                                <tr>
                                                    <td>23604094</td>
                                                    <td>
                                                        Invest Management
                                                        Dashboard - Regular
                                                        License
                                                    </td>
                                                    <td>$300.00</td>
                                                    <td>1</td>
                                                    <td>$300.00</td>
                                                </tr>
                                                <tr>
                                                    <td>23604094</td>
                                                    <td>
                                                        6 months premium support
                                                    </td>
                                                    <td>$400.00</td>
                                                    <td>1</td>
                                                    <td>$400.00</td>
                                                </tr>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td colSpan="2"></td>
                                                    <td colSpan="2">
                                                        Subtotal
                                                    </td>
                                                    <td>$900</td>
                                                </tr>
                                                <tr>
                                                    <td colSpan="2"></td>
                                                    <td colSpan="2">
                                                        Processing fee
                                                    </td>
                                                    <td>$10.00</td>
                                                </tr>
                                                <tr>
                                                    <td colSpan="2"></td>
                                                    <td colSpan="2">TAX</td>
                                                    <td>$50.00</td>
                                                </tr>
                                                <tr>
                                                    <td colSpan="2"></td>
                                                    <td colSpan="2">
                                                        Grand Total
                                                    </td>
                                                    <td>$89770.00</td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                        <div className="nk-notes ff-italic fs-12px text-soft">
                                            Orders was created on a computer and
                                            is valid without the signature and
                                            seal.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Block>
                </Content>
            )}
        </React.Fragment>
    );
};

Show.propTypes = {
    match: PropTypes.object,
    pageTitle: PropTypes.string.isRequired
};

export default Show;
