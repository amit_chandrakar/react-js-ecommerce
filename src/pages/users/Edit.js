import React, { useEffect, useState } from 'react';
import Content from '../../layout/content/Content';
import Head from '../../layout/head/Head';
import {
    Block,
    BlockDes,
    PreviewCard,
    BlockHead,
    BlockHeadContent,
    BlockBetween,
    Icon
} from '../../components/Component';
import { useForm } from 'react-hook-form';
import classNames from 'classnames';
import { Row, Col, Label, Form, Button } from 'reactstrap';
import axios from 'axios';
import { ToastContainer } from 'react-toastify';
import { APP_URL, formatDate } from '../../utils/Constants';
import { showToast } from '../../utils/Utils';
import DatePicker from 'react-datepicker';
import { useHistory, useParams } from 'react-router';
import PropTypes from 'prop-types';
import { useTitle } from '../../contexts/TitleContext';

const Edit = ({ pageTitle }) => {
    const history = useHistory();
    const { errors, register, handleSubmit } = useForm();
    const formClass = classNames({
        'form-validate': true,
        'is-alter': true
    });
    const [DOB, setDOB] = useState();
    const [data, setData] = useState(0);
    // Category id from params
    const { id } = useParams();

    const { setDocumentTitle } = useTitle();
    useEffect(() => {
        setDocumentTitle(pageTitle);
    }, [pageTitle, setDocumentTitle]);

    const fetchUser = async () => {
        try {
            const response = await axios.get(`${APP_URL}/users/edit/${id}`);
            setData(response.data.data.user);

            // Check if dob is not null or undefined
            if (response.data.data.user.dob !== null && response.data.data.user.dob !== undefined) {
                setDOB(new Date(response.data.data.user.dob));
            }
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }
    };

    // Function to get all the categories on page load
    useEffect(() => {
        fetchUser();
    }, []);

    const onFormSubmit = async (e) => {
        // Generate a companyId in ObjectId form
        e.companyId = '5f9d88f9d4b7a1b2c8c7e8b1';

        // Check if dob is not null or undefined
        if (DOB !== null && DOB !== undefined) {
            e.dob = DOB.toISOString();
        }

        try {
            const response = await axios.put(`${APP_URL}/users/update/${id}`, e);

            showToast('success', response.data.message, () => {
                history.push(`${process.env.PUBLIC_URL}/account/users`);
            });
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }
    };

    return (
        <React.Fragment>
            <Head title={pageTitle}></Head>
            <Content page="component" CardSize="xl">
                <BlockHead size="sm">
                    <BlockBetween>
                        <BlockHeadContent>
                            <BlockDes className="text-soft">
                                <ul className="list-inline">
                                    <li>
                                        Created On:{' '}
                                        <span className="text-base">
                                            { formatDate(data.createdAt) }
                                        </span>
                                    </li>
                                    <li>
                                        Last Updated:{' '}
                                        <span className="text-base">
                                            { data.updatedAt === null || data.updatedAt === undefined ? 'N/A' : formatDate(data.updatedAt) }
                                        </span>
                                    </li>
                                    <li>
                                        Last Login:{' '}
                                        <span className="text-base">
                                            { data.lastLoginAt === null || data.lastLoginAt === undefined ? 'N/A' : formatDate(data.lastLoginAt) }
                                        </span>
                                    </li>

                                    {
                                        data.deletedAt === null || data.deletedAt === undefined
                                            ? ''
                                            : (
                                                <li>
                                            Deleted On: {' '}
                                                    <span className="text-base">
                                                        { formatDate(data.deletedAt) }
                                                    </span>
                                                </li>)
                                    }

                                </ul>
                            </BlockDes>
                        </BlockHeadContent>
                        <BlockHeadContent>
                            <Button
                                color="light"
                                outline
                                className="bg-white d-none d-sm-inline-flex"
                                onClick={() => history.goBack()}
                            >
                                <Icon name="arrow-left"></Icon>
                                <span>Back</span>
                            </Button>
                            <a
                                href="#back"
                                onClick={(ev) => {
                                    ev.preventDefault();
                                    history.goBack();
                                }}
                                className="btn btn-icon btn-outline-light bg-white d-inline-flex d-sm-none"
                            >
                                <Icon name="arrow-left"></Icon>
                            </a>
                        </BlockHeadContent>
                    </BlockBetween>
                </BlockHead>

                <Block size="sm">
                    <PreviewCard>
                        <Form
                            className={formClass}
                            onSubmit={handleSubmit(onFormSubmit)}
                        >
                            <Row className="g-gs">
                                <Col md="3">
                                    <div className="form-group">
                                        <Label
                                            className="form-label"
                                            htmlFor="fv-name"
                                        >
                                            Full Name
                                        </Label>
                                        <div className="form-control-wrap">
                                            <input
                                                ref={register({
                                                    required: true
                                                })}
                                                type="text"
                                                id="fv-name"
                                                name="name"
                                                className="form-control"
                                                defaultValue={data.name}
                                            />
                                            {errors.name && (
                                                <span className="invalid">
                                                    This field is required
                                                </span>
                                            )}
                                        </div>
                                    </div>
                                </Col>
                                <Col md="3">
                                    <div className="form-group">
                                        <Label
                                            className="form-label"
                                            htmlFor="fv-email"
                                        >
                                            Email
                                        </Label>
                                        <div className="form-control-wrap">
                                            <input
                                                ref={register({
                                                    required: true,
                                                    pattern: {
                                                        value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                                                        message:
                                                            'Invalid email address'
                                                    }
                                                })}
                                                type="email"
                                                id="fv-email"
                                                name="email"
                                                className="form-control"
                                                defaultValue={data.email}
                                            />
                                            {errors.email &&
                                                errors.email.type ===
                                                    'required' && (
                                                <span className="invalid">
                                                        This is required
                                                </span>
                                            )}
                                            {errors.email &&
                                                errors.email.type ===
                                                    'pattern' && (
                                                <span className="invalid">
                                                    {errors.email.message}
                                                </span>
                                            )}
                                        </div>
                                    </div>
                                </Col>
                                <Col md="3">
                                    <div className="form-group">
                                        <Label
                                            className="form-label"
                                            htmlFor="fv-password"
                                        >
                                            Password
                                        </Label>
                                        <div className="form-control-wrap">
                                            <input
                                                ref={register({
                                                    required: true
                                                })}
                                                type="text"
                                                id="fv-password"
                                                name="password"
                                                className="form-control"
                                                defaultValue={data.password}
                                            />
                                            {errors.password && (
                                                <span className="invalid">
                                                    This field is required
                                                </span>
                                            )}
                                        </div>
                                    </div>
                                </Col>
                                <Col md="3">
                                    <div className="form-group">
                                        <Label
                                            className="form-label"
                                            htmlFor="fv-mobile"
                                        >
                                            Mobile
                                        </Label>
                                        <div className="form-control-wrap">
                                            <input
                                                ref={register({
                                                    required: true
                                                })}
                                                type="text"
                                                id="fv-mobile"
                                                name="mobile"
                                                className="form-control"
                                                defaultValue={data.mobile}
                                            />
                                            {errors.mobile && (
                                                <span className="invalid">
                                                    This field is required
                                                </span>
                                            )}
                                        </div>
                                    </div>
                                </Col>
                                <Col sm="3">
                                    <div className="form-group">
                                        <Label>DOB</Label>
                                        <div className="form-control-wrap">
                                            <DatePicker
                                                selected={DOB}
                                                onChange={setDOB}
                                                className="form-control date-picker"
                                                name="dob"
                                            />
                                        </div>
                                    </div>
                                </Col>
                                <Col md="3">
                                    <div className="form-group">
                                        <Label
                                            className="form-label"
                                            htmlFor="fv-gender"
                                        >
                                            Gender
                                        </Label>
                                        <div className="form-control-wrap">
                                            <div className="form-control-select">
                                                <select
                                                    className="form-control form-select"
                                                    id="fv-gender"
                                                    name="gender"
                                                    placeholder="Select a option"
                                                >
                                                    <option
                                                        label="Select Gender"
                                                        value=""
                                                        selected={data.gender === ''}
                                                    ></option>
                                                    <option value="male" selected={data.gender === 'male'}>
                                                        Male
                                                    </option>
                                                    <option value="female" selected={data.gender === 'female'}>
                                                        Female
                                                    </option>
                                                    <option value="other" selected={data.gender === 'other'}>
                                                        Other
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </Col>
                                <Col md="3">
                                    <div className="form-group">
                                        <Label
                                            className="form-label"
                                            htmlFor="fv-role"
                                        >
                                            Role
                                        </Label>
                                        <div className="form-control-wrap">
                                            <div className="form-control-select">
                                                <select
                                                    ref={register({
                                                        required: true
                                                    })}
                                                    className="form-control form-select"
                                                    id="fv-role"
                                                    name="role"
                                                    placeholder="Select a option"
                                                >
                                                    <option value="customer" selected={data.role === 'customer'}>
                                                        Customer
                                                    </option>
                                                    <option value="admin" selected={data.role === 'admin'}>
                                                        Admin
                                                    </option>
                                                </select>
                                                {errors.role && (
                                                    <span className="invalid">
                                                        This field is required
                                                    </span>
                                                )}
                                            </div>
                                        </div>
                                    </div>
                                </Col>
                                <Col md="3">
                                    <div className="form-group">
                                        <Label
                                            className="form-label"
                                            htmlFor="fv-status"
                                        >
                                            Status
                                        </Label>
                                        <div className="form-control-wrap">
                                            <div className="form-control-select">
                                                <select
                                                    ref={register({
                                                        required: true
                                                    })}
                                                    className="form-control form-select"
                                                    id="fv-status"
                                                    name="status"
                                                    placeholder="Select a option"
                                                >
                                                    <option
                                                        label="Select status"
                                                        value=""
                                                        selected={
                                                            data.status === ''
                                                        }
                                                    ></option>
                                                    <option
                                                        value="active"
                                                        selected={
                                                            data.status ===
                                                            'active'
                                                        }
                                                    >
                                                        Active
                                                    </option>
                                                    <option
                                                        value="inactive"
                                                        selected={
                                                            data.status ===
                                                            'inactive'
                                                        }
                                                    >
                                                        Inactive
                                                    </option>
                                                </select>
                                                {errors.status && (
                                                    <span className="invalid">
                                                        This field is required
                                                    </span>
                                                )}
                                            </div>
                                        </div>
                                    </div>
                                </Col>
                                <Col md="12">
                                    <div className="form-group">
                                        <Button color="primary">
                                            <Icon name="check"></Icon>
                                            <span>Save</span>
                                        </Button>
                                    </div>
                                </Col>
                            </Row>
                        </Form>
                    </PreviewCard>
                </Block>
            </Content>
            <ToastContainer />
        </React.Fragment>
    );
};

Edit.propTypes = {
    pageTitle: PropTypes.string.isRequired
};

export default Edit;
