import React, { useEffect } from 'react';
import Content from '../../layout/content/Content';
import Head from '../../layout/head/Head';
import {
    Block,
    PreviewCard,
    BlockHead,
    BlockHeadContent,
    BlockBetween,
    Icon
} from '../../components/Component';
import { useHistory } from 'react-router';
import { useForm } from 'react-hook-form';
import classNames from 'classnames';
import { Row, Col, Label, Form, Button } from 'reactstrap';
import axios from 'axios';
import { ToastContainer } from 'react-toastify';
import { APP_URL } from '../../utils/Constants';
import { showToast } from '../../utils/Utils';
import PropTypes from 'prop-types';
import { useTitle } from '../../contexts/TitleContext';

const Create = ({ pageTitle }) => {
    const history = useHistory();
    const { errors, register, handleSubmit } = useForm();

    const formClass = classNames({
        'form-validate': true,
        'is-alter': true
    });

    const { setDocumentTitle } = useTitle();
    useEffect(() => {
        setDocumentTitle(pageTitle);
    }, [pageTitle, setDocumentTitle]);

    const onFormSubmit = async (e) => {
        // Generate a companyId in ObjectId form
        e.companyId = '5f9d88f9d4b7a1b2c8c7e8b1';

        try {
            const response = await axios.post(`${APP_URL}/coupons/store`, e);

            showToast('success', response.data.message, () => {
                history.push(`${process.env.PUBLIC_URL}/account/coupons`);
            });
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }
    };

    return (
        <React.Fragment>
            <Head title={pageTitle}></Head>
            <Content page="component" CardSize="xl">
                <BlockHead size="sm">
                    <BlockBetween>
                        <BlockHeadContent></BlockHeadContent>
                        <BlockHeadContent>
                            <Button
                                color="light"
                                outline
                                className="bg-white d-none d-sm-inline-flex"
                                onClick={() => history.goBack()}
                            >
                                <Icon name="arrow-left"></Icon>
                                <span>Back</span>
                            </Button>
                            <a
                                href="#back"
                                onClick={(ev) => {
                                    ev.preventDefault();
                                    history.goBack();
                                }}
                                className="btn btn-icon btn-outline-light bg-white d-inline-flex d-sm-none"
                            >
                                <Icon name="arrow-left"></Icon>
                            </a>
                        </BlockHeadContent>
                    </BlockBetween>
                </BlockHead>

                <Block size="sm">
                    <PreviewCard>
                        <Form
                            className={formClass}
                            onSubmit={handleSubmit(onFormSubmit)}
                        >
                            <Row className="g-gs">
                                <Col md="6">
                                    <div className="form-group">
                                        <Label
                                            className="form-label"
                                            htmlFor="fv-code"
                                        >
                                            Coupon Code
                                        </Label>
                                        <div className="form-control-wrap">
                                            <input
                                                ref={register({
                                                    required: true
                                                })}
                                                type="text"
                                                id="fv-code"
                                                name="code"
                                                className="form-control"
                                                placeholder="e.g. 10OFF"
                                            />
                                            {errors.code && (
                                                <span className="invalid">
                                                    This field is required
                                                </span>
                                            )}
                                        </div>
                                    </div>
                                </Col>

                                <Col md="6">
                                    <div className="form-group">
                                        <Label
                                            className="form-label"
                                            htmlFor="fv-discount"
                                        >
                                            Discount
                                        </Label>
                                        <div className="form-control-wrap">
                                            <input
                                                ref={register({
                                                    required: true
                                                })}
                                                type="text"
                                                id="fv-discount"
                                                name="discount"
                                                className="form-control"
                                                placeholder="e.g. 10"
                                            />
                                            {errors.discount && (
                                                <span className="invalid">
                                                    This field is required
                                                </span>
                                            )}
                                        </div>
                                    </div>
                                </Col>

                                <Col md="6">
                                    <div className="form-group">
                                        <Label
                                            className="form-label"
                                            htmlFor="fv-status"
                                        >
                                            Discount Type
                                        </Label>
                                        <div className="form-control-wrap">
                                            <div className="form-control-select">
                                                <select
                                                    ref={register({
                                                        required: true
                                                    })}
                                                    className="form-control form-select"
                                                    id="fv-discountType"
                                                    name="discountType"
                                                    placeholder="Select a option"
                                                >
                                                    <option
                                                        label="Select Discount Type"
                                                        value=""
                                                    ></option>
                                                    <option value="flat">
                                                        Flat
                                                    </option>
                                                    <option value="percentage">
                                                        Percentage
                                                    </option>
                                                </select>
                                                {errors.discountType && (
                                                    <span className="invalid">
                                                        This field is required
                                                    </span>
                                                )}
                                            </div>
                                        </div>
                                    </div>
                                </Col>

                                <Col md="6">
                                    <div className="form-group">
                                        <Label
                                            className="form-label"
                                            htmlFor="fv-status"
                                        >
                                            Status
                                        </Label>
                                        <div className="form-control-wrap">
                                            <div className="form-control-select">
                                                <select
                                                    ref={register({
                                                        required: true
                                                    })}
                                                    className="form-control form-select"
                                                    id="fv-status"
                                                    name="status"
                                                    placeholder="Select a option"
                                                >
                                                    <option
                                                        label="Select status"
                                                        value=""
                                                    ></option>
                                                    <option value="active">
                                                        Active
                                                    </option>
                                                    <option value="inactive">
                                                        Inactive
                                                    </option>
                                                </select>
                                                {errors.status && (
                                                    <span className="invalid">
                                                        This field is required
                                                    </span>
                                                )}
                                            </div>
                                        </div>
                                    </div>
                                </Col>

                                <Col md="12">
                                    <div className="form-group">
                                        <Button color="primary">
                                            <Icon name="check"></Icon>
                                            <span>Save</span>
                                        </Button>
                                    </div>
                                </Col>
                            </Row>
                        </Form>
                    </PreviewCard>
                </Block>
            </Content>
            <ToastContainer />
        </React.Fragment>
    );
};

Create.propTypes = {
    pageTitle: PropTypes.string.isRequired
};

export default Create;
