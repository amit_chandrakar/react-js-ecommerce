import React, { useEffect, useState } from 'react';
import Content from '../../layout/content/Content';
import Head from '../../layout/head/Head';
import {
    Block,
    PreviewCard,
    BlockHead,
    BlockHeadContent,
    BlockBetween,
    Icon
} from '../../components/Component';
import { useHistory } from 'react-router';
import { useForm } from 'react-hook-form';
import classNames from 'classnames';
import { Row, Col, Label, Form, Button } from 'reactstrap';
import axios from 'axios';
import { ToastContainer } from 'react-toastify';
import { showToast } from '../../utils/Utils';
import { APP_URL, slugify } from '../../utils/Constants';
import QuillComponent from '../../components/partials/rich-editor/QuillComponent';
import PropTypes from 'prop-types';
import { useTitle } from '../../contexts/TitleContext';
import Tags from '@yaireo/tagify/dist/react.tagify';

const tagifySettings = {
    blacklist: ['xxx', 'yyy', 'zzz'],
    // maxTags: 6,
    // backspace: 'edit',
    addTagOnBlur: false,
    placeholder: 'E.g. Apple, iPhone, 11 Pro Max, Mobile, etc.',
    dropdown: {
        enabled: 1 // shows suggestions dropdown
    }
};

const Create = ({ pageTitle }) => {
    const history = useHistory();
    const { errors, register, handleSubmit } = useForm();
    const formClass = classNames({
        'form-validate': true,
        'is-alter': true
    });
    const [categories, setCategories] = useState([]);
    const [subCategories, setSubCategories] = useState([]);
    const [brands, setBrands] = useState([]);
    const [longDescriptionText, setLongDescriptionText] = useState('');
    const [tagifyOptions, setTagifyOptions] = useState([]);

    const { setDocumentTitle } = useTitle();
    useEffect(() => {
        setDocumentTitle(pageTitle);
    }, [pageTitle, setDocumentTitle]);

    const fetchCategories = async () => {
        let response;

        try {
            response = await axios.get(`${APP_URL}/categories`);
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }

        setCategories(response.data.categories);
    };

    const fetchSubCategories = async (id) => {
        let response;

        try {
            response = await axios.get(
                `${APP_URL}/sub-categories/get-sub-categories-by-category-id/${id}`
            );
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }

        setSubCategories(response.data.subCategories);
    };

    const fetchBrands = async () => {
        let response;

        try {
            response = await axios.get(`${APP_URL}/brands`);
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }

        setBrands(response.data.brands);
    };

    // Function to get all the categories on page load
    useEffect(() => {
        fetchCategories();
        fetchBrands();
    }, []);

    const onTagifyChange = (e) => {
        if (e.detail.value !== '') {
            const arrayVal = JSON.parse(e.detail.value);
            setTagifyOptions(arrayVal);
        } else {
            setTagifyOptions(['']);
        }
    };

    const settings = {
        ...tagifySettings,
        addTagOnBlur: true
    };

    const onFormSubmit = async (e) => {
        // Generate a companyId in ObjectId form
        e.companyId = '5f9d88f9d4b7a1b2c8c7e8b1';
        e.longDescription = longDescriptionText;
        // Create product name slug
        e.slug = slugify(e.name);

        // Create tags array
        e.tags = [];
        if (tagifyOptions.length > 0) {
            e.tags = tagifyOptions.map((tag) => tag.value);
        }

        try {
            const response = await axios.post(`${APP_URL}/products/store`, e);

            showToast('success', response.data.message, () => {
                history.push(`${process.env.PUBLIC_URL}/account/products`);
            });
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }
    };

    return (
        <React.Fragment>
            <Head title={pageTitle}></Head>
            <Content page="component" CardSize="xl">
                <BlockHead size="sm">
                    <BlockBetween>
                        <BlockHeadContent></BlockHeadContent>
                        <BlockHeadContent>
                            <Button
                                color="light"
                                outline
                                className="bg-white d-none d-sm-inline-flex"
                                onClick={() => history.goBack()}
                            >
                                <Icon name="arrow-left"></Icon>
                                <span>Back</span>
                            </Button>
                            <a
                                href="#back"
                                onClick={(ev) => {
                                    ev.preventDefault();
                                    history.goBack();
                                }}
                                className="btn btn-icon btn-outline-light bg-white d-inline-flex d-sm-none"
                            >
                                <Icon name="arrow-left"></Icon>
                            </a>
                        </BlockHeadContent>
                    </BlockBetween>
                </BlockHead>

                <Block size="sm">
                    <PreviewCard>
                        <Form
                            className={formClass}
                            onSubmit={handleSubmit(onFormSubmit)}
                        >
                            <Row className="g-gs">

                                <Col md="3">
                                    <div className="form-group">
                                        <Label
                                            className="form-label"
                                            htmlFor="fv-categoryId"
                                        >
                                            Category
                                        </Label>
                                        <div className="form-control-wrap">
                                            <div className="form-control-select">
                                                <select
                                                    ref={register({
                                                        required: true
                                                    })}
                                                    className="form-control form-select"
                                                    id="fv-categoryId"
                                                    name="categoryId"
                                                    placeholder="Select a option"
                                                    onChange={(e) => {
                                                        fetchSubCategories(
                                                            e.target.value
                                                        );
                                                    }}
                                                >
                                                    <option
                                                        label="Select"
                                                        value=""
                                                    ></option>
                                                    {categories.map(
                                                        (category) => (
                                                            <option
                                                                key={
                                                                    category._id
                                                                }
                                                                value={
                                                                    category._id
                                                                }
                                                            >
                                                                {' '}
                                                                {
                                                                    category.name
                                                                }{' '}
                                                            </option>
                                                        )
                                                    )}
                                                </select>
                                                {errors.categoryId && (
                                                    <span className="invalid">
                                                        This field is required
                                                    </span>
                                                )}
                                            </div>
                                        </div>
                                    </div>
                                </Col>

                                <Col md="3">
                                    <div className="form-group">
                                        <Label
                                            className="form-label"
                                            htmlFor="fv-subCategoryId"
                                        >
                                            Sub Category
                                        </Label>
                                        <div className="form-control-wrap">
                                            <div className="form-control-select">
                                                <select
                                                    ref={register({
                                                        required: true
                                                    })}
                                                    className="form-control form-select"
                                                    id="fv-subCategoryId"
                                                    name="subCategoryId"
                                                    placeholder="Select a option"
                                                >
                                                    <option
                                                        label="Select"
                                                        value=""
                                                    ></option>
                                                    {subCategories.map(
                                                        (category) => (
                                                            <option
                                                                key={
                                                                    category._id
                                                                }
                                                                value={
                                                                    category._id
                                                                }
                                                            >
                                                                {' '}
                                                                {
                                                                    category.name
                                                                }{' '}
                                                            </option>
                                                        )
                                                    )}
                                                </select>
                                                {errors.subCategoryId && (
                                                    <span className="invalid">
                                                        This field is required
                                                    </span>
                                                )}
                                            </div>
                                        </div>
                                    </div>
                                </Col>

                                <Col md="3">
                                    <div className="form-group">
                                        <Label
                                            className="form-label"
                                            htmlFor="fv-brandId"
                                        >
                                            Brands
                                        </Label>
                                        <div className="form-control-wrap">
                                            <div className="form-control-select">
                                                <select
                                                    ref={register({
                                                        required: true
                                                    })}
                                                    className="form-control form-select"
                                                    id="fv-brandId"
                                                    name="brandId"
                                                    placeholder="Select a option"
                                                >
                                                    <option
                                                        label="Select"
                                                        value=""
                                                    ></option>
                                                    {brands.map((brand) => (
                                                        <option
                                                            key={brand._id}
                                                            value={brand._id}
                                                        >
                                                            {' '}
                                                            {brand.name}{' '}
                                                        </option>
                                                    ))}
                                                </select>
                                                {errors.brandId && (
                                                    <span className="invalid">
                                                        This field is required
                                                    </span>
                                                )}
                                            </div>
                                        </div>
                                    </div>
                                </Col>

                                <Col md="3">
                                    <div className="form-group">
                                        <Label
                                            className="form-label"
                                            htmlFor="fv-status"
                                        >
                                            Status
                                        </Label>
                                        <div className="form-control-wrap">
                                            <div className="form-control-select">
                                                <select
                                                    ref={register({
                                                        required: true
                                                    })}
                                                    className="form-control form-select"
                                                    id="fv-status"
                                                    name="status"
                                                    placeholder="Select a option"
                                                >
                                                    <option
                                                        label="Select status"
                                                        value=""
                                                    ></option>
                                                    <option value="active">
                                                        Active
                                                    </option>
                                                    <option value="inactive">
                                                        Inactive
                                                    </option>
                                                </select>
                                                {errors.status && (
                                                    <span className="invalid">
                                                        This field is required
                                                    </span>
                                                )}
                                            </div>
                                        </div>
                                    </div>
                                </Col>

                                <Col md="12">
                                    <div className="form-group">
                                        <Label
                                            className="form-label"
                                            htmlFor="fv-name"
                                        >
                                            Product Name
                                        </Label>
                                        <div className="form-control-wrap">
                                            <input
                                                ref={register({
                                                    required: true
                                                })}
                                                type="text"
                                                id="fv-name"
                                                name="name"
                                                className="form-control"
                                                placeholder="e.g. Apple iPhone 11 Pro Max"
                                            />
                                            {errors.name && (
                                                <span className="invalid">
                                                    This field is required
                                                </span>
                                            )}
                                        </div>
                                    </div>
                                </Col>

                                <Col md="12">
                                    <div className="form-group">
                                        <Label
                                            className="form-label"
                                            htmlFor="fv-shortDescription"
                                        >
                                            Short Product Description
                                        </Label>
                                        <div className="form-control-wrap">
                                            <input
                                                ref={register({
                                                    required: true
                                                })}
                                                type="text"
                                                id="fv-shortDescription"
                                                name="shortDescription"
                                                className="form-control"
                                                placeholder="e.g. Apple iPhone 11 Pro Max"
                                            />
                                            {errors.shortDescription && (
                                                <span className="invalid">
                                                    This field is required
                                                </span>
                                            )}
                                        </div>
                                    </div>
                                </Col>

                                <Col md="12">
                                    <div className="form-group">
                                        <Label
                                            className="form-label"
                                        >
                                            Long Product Description
                                        </Label>
                                        <div className="form-control-wrap">
                                            <QuillComponent placeholder="e.g. This is best product in mobile category...!" onChange={setLongDescriptionText} />
                                        </div>
                                    </div>
                                </Col>

                                <Col md="3">
                                    <div className="form-group">
                                        <Label
                                            className="form-label"
                                            htmlFor="fv-price"
                                        >
                                            Price
                                        </Label>
                                        <div className="form-control-wrap">
                                            <input
                                                ref={register({
                                                    required: true
                                                })}
                                                type="text"
                                                id="fv-price"
                                                name="price"
                                                className="form-control"
                                                placeholder="e.g. 1000"
                                            />
                                            {errors.price && (
                                                <span className="invalid">
                                                    This field is required
                                                </span>
                                            )}
                                        </div>
                                    </div>
                                </Col>

                                <Col md="3">
                                    <div className="form-group">
                                        <Label
                                            className="form-label"
                                            htmlFor="fv-discount"
                                        >
                                            Discount Price
                                        </Label>
                                        <div className="form-control-wrap">
                                            <input
                                                type="text"
                                                id="fv-discount"
                                                name="discount"
                                                className="form-control"
                                                placeholder="e.g. 800"
                                            />
                                        </div>
                                    </div>
                                </Col>

                                <Col md="3">
                                    <div className="form-group">
                                        <Label
                                            className="form-label"
                                            htmlFor="fv-quantityInStock"
                                        >
                                            Quantity In Stock
                                        </Label>
                                        <div className="form-control-wrap">
                                            <input
                                                ref={register({
                                                    required: true
                                                })}
                                                type="text"
                                                id="fv-quantityInStock"
                                                name="quantityInStock"
                                                className="form-control"
                                                placeholder="e.g. 109"
                                            />
                                            {errors.quantityInStock && (
                                                <span className="invalid">
                                                    This field is required
                                                </span>
                                            )}
                                        </div>
                                    </div>
                                </Col>

                                <Col md="3">
                                    <div className="form-group">
                                        <Label
                                            className="form-label"
                                            htmlFor="fv-displayOrder"
                                        >
                                            Display Order
                                        </Label>
                                        <div className="form-control-wrap">
                                            <input
                                                ref={register({
                                                    required: true
                                                })}
                                                type="text"
                                                id="fv-displayOrder"
                                                name="displayOrder"
                                                className="form-control"
                                                placeholder="e.g. 1"
                                            />
                                            {errors.displayOrder && (
                                                <span className="invalid">
                                                    This field is required
                                                </span>
                                            )}
                                        </div>
                                    </div>
                                </Col>

                                <Col md="3">
                                    <div className="form-group">
                                        <Label
                                            className="form-label"
                                            htmlFor="fv-sku"
                                        >
                                            SKU
                                        </Label>
                                        <div className="form-control-wrap">
                                            <input
                                                ref={register({
                                                    required: true
                                                })}
                                                type="text"
                                                id="fv-sku"
                                                name="sku"
                                                className="form-control"
                                                placeholder="e.g. KHJYGFUF6676"
                                            />
                                            {errors.sku && (
                                                <span className="invalid">
                                                    This field is required
                                                </span>
                                            )}
                                        </div>
                                    </div>
                                </Col>

                                <Col md="9">
                                    <div className="form-group">
                                        <Label
                                            className="form-label"
                                            htmlFor="fv-sku"
                                        >
                                            Tags
                                        </Label>
                                        <div className="form-control-wrap">
                                            <Tags
                                                className="form-control"
                                                mode="textarea"
                                                value={tagifyOptions}
                                                onChange={(e) => onTagifyChange(e)}
                                                settings={settings}
                                                showDropdown={false}
                                            />
                                        </div>
                                    </div>
                                </Col>

                                <Col md="12">
                                    <div className="form-group">
                                        <Button color="primary">
                                            <Icon name="check"></Icon>
                                            <span>Save</span>
                                        </Button>
                                    </div>
                                </Col>
                            </Row>
                        </Form>
                    </PreviewCard>
                </Block>
            </Content>
            <ToastContainer />
        </React.Fragment>
    );
};

Create.propTypes = {
    pageTitle: PropTypes.string.isRequired
};

export default Create;
