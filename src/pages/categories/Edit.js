import React, { useEffect, useState } from 'react';
import Content from '../../layout/content/Content';
import Head from '../../layout/head/Head';
import {
    Block,
    PreviewCard,
    BlockHead,
    BlockHeadContent,
    BlockBetween,
    Icon
} from '../../components/Component';
import { useHistory, useParams } from 'react-router';
import { useForm } from 'react-hook-form';
import classNames from 'classnames';
import { Row, Col, Label, Form, Button } from 'reactstrap';
import axios from 'axios';
import { ToastContainer } from 'react-toastify';
import { APP_URL, slugify } from '../../utils/Constants';
import { showToast } from '../../utils/Utils';
import PropTypes from 'prop-types';
import { useTitle } from '../../contexts/TitleContext';

const Edit = ({ pageTitle }) => {
    const history = useHistory();
    const { errors, register, handleSubmit } = useForm();
    const [data, setData] = useState({});
    const formClass = classNames({
        'form-validate': true,
        'is-alter': true
    });

    // Category id from params
    const { id } = useParams();

    const { setDocumentTitle } = useTitle();
    useEffect(() => {
        setDocumentTitle(pageTitle);
    }, [pageTitle, setDocumentTitle]);

    const getCategories = async () => {
        let response;

        try {
            response = await axios.get(
                `${APP_URL}/categories/edit/${id}`
            );
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }

        setData(response.data.data.category);
    };

    useEffect(() => {
        // Fetching data from api
        getCategories();
    }, []);

    const onFormSubmit = async (e) => {
        e.slug = slugify(e.name);
        // Generate a companyId in ObjectId form
        e.companyId = '5f9d88f9d4b7a1b2c8c7e8b1';

        try {
            const response = await axios.put(`${APP_URL}/categories/update/${data._id}`, e);

            showToast('success', response.data.message, () => {
                history.push(`${process.env.PUBLIC_URL}/account/categories`);
            });
        } catch (error) {
            showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
        }
    };

    return (
        <React.Fragment>
            <Head title={pageTitle}></Head>
            <Content page="component" CardSize="xl">
                <BlockHead size="sm">
                    <BlockBetween>
                        <BlockHeadContent></BlockHeadContent>
                        <BlockHeadContent>
                            <Button
                                color="light"
                                outline
                                className="bg-white d-none d-sm-inline-flex"
                                onClick={() => history.goBack()}
                            >
                                <Icon name="arrow-left"></Icon>
                                <span>Back</span>
                            </Button>
                            <a
                                href="#back"
                                onClick={(ev) => {
                                    ev.preventDefault();
                                    history.goBack();
                                }}
                                className="btn btn-icon btn-outline-light bg-white d-inline-flex d-sm-none"
                            >
                                <Icon name="arrow-left"></Icon>
                            </a>
                        </BlockHeadContent>
                    </BlockBetween>
                </BlockHead>

                <Block size="sm">
                    <PreviewCard>
                        <Form
                            className={formClass}
                            onSubmit={handleSubmit(onFormSubmit)}
                        >
                            <Row className="g-gs">
                                <Col md="6">
                                    <div className="form-group">
                                        <Label
                                            className="form-label"
                                            htmlFor="fv-name"
                                        >
                                            Name
                                        </Label>
                                        <div className="form-control-wrap">
                                            <input
                                                ref={register({
                                                    required: true
                                                })}
                                                type="text"
                                                id="fv-name"
                                                name="name"
                                                className="form-control"
                                                value={data.name}
                                                onChange={(e) =>
                                                    setData({
                                                        ...data,
                                                        name: e.target.value
                                                    })
                                                }
                                            />
                                            {errors.name && (
                                                <span className="invalid">
                                                    This field is required
                                                </span>
                                            )}
                                        </div>
                                    </div>
                                </Col>
                                <Col md="6">
                                    <div className="form-group">
                                        <Label
                                            className="form-label"
                                            htmlFor="fv-status"
                                        >
                                            Status
                                        </Label>
                                        <div className="form-control-wrap">
                                            <div className="form-control-select">
                                                <select
                                                    ref={register({
                                                        required: true
                                                    })}
                                                    className="form-control form-select"
                                                    id="fv-status"
                                                    name="status"
                                                    placeholder="Select a option"
                                                >
                                                    <option
                                                        label="Select status"
                                                        value=""
                                                        selected={data.status === ''}
                                                    ></option>
                                                    <option value="active" selected={data.status === 'active'}>
                                                        Active
                                                    </option>
                                                    <option value="inactive" selected={data.status === 'inactive'}>
                                                        Inactive
                                                    </option>
                                                </select>
                                                {errors.status && (
                                                    <span className="invalid">
                                                        This field is required
                                                    </span>
                                                )}
                                            </div>
                                        </div>
                                    </div>
                                </Col>
                                <Col md="12">
                                    <div className="form-group">
                                        <Button color="primary">
                                            <Icon name="check"></Icon>
                                            <span>Save</span>
                                        </Button>
                                    </div>
                                </Col>
                            </Row>
                        </Form>
                    </PreviewCard>
                </Block>
            </Content>
            <ToastContainer />
        </React.Fragment>
    );
};

Edit.propTypes = {
    pageTitle: PropTypes.string.isRequired
};

export default Edit;
