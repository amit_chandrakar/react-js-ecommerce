import React, { useState } from 'react';
import SyntaxHighlighter from 'react-syntax-highlighter';
import { Card, Button } from 'reactstrap';
import { a11yLight } from 'react-syntax-highlighter/dist/esm/styles/hljs';
import OverlineTitle from '../text/Text';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import PropTypes from 'prop-types';

const PreviewCard = ({ className, bodyClass, ...props }) => {
    return (
        <Card className={`card-preview ${className || ''}`}>
            <div className={`card-inner ${bodyClass || ''}`}>{props.children}</div>
        </Card>
    );
};

PreviewCard.propTypes = {
    className: PropTypes.string,
    bodyClass: PropTypes.string,
    children: PropTypes.node
};

const PreviewAltCard = ({ className, bodyClass, ...props }) => {
    return (
        <Card className={`card-bordered ${className || ''}`}>
            <div className={`card-inner ${bodyClass || ''}`}>{props.children}</div>
        </Card>
    );
};

PreviewAltCard.propTypes = {
    className: PropTypes.string,
    bodyClass: PropTypes.string,
    children: PropTypes.node
};

const PreviewTable = ({ ...props }) => {
    return (
        <Card className="card-preview">
            <table className="table preview-reference">{props.children}</table>
        </Card>
    );
};

PreviewTable.propTypes = {
    children: PropTypes.node
};

const CodeBlock = ({ language, ...props }) => {
    const [copyText] = useState(props.children);
    const [copyState, setCopyState] = useState(false);
    const onCopyClick = () => {
        setCopyState(true);
        setTimeout(() => setCopyState(false), 2000);
    };
    return (
        <div className={`code-block code-block-clean ${copyState ? 'clipboard-success' : ''}`}>
            <OverlineTitle className="title">{props.title ? props.title : 'Code Example'}</OverlineTitle>
            <CopyToClipboard text={copyText} onCopy={onCopyClick}>
                <Button color="blank" size="sm" className="clipboard-init">
                    {copyState ? 'Copied' : 'Copy'}
                </Button>
            </CopyToClipboard>
            <SyntaxHighlighter language="javascript" className="bg-lighter h-max-150px m-0" style={a11yLight}>
                {props.children}
            </SyntaxHighlighter>
        </div>
    );
};

CodeBlock.propTypes = {
    language: PropTypes.string,
    title: PropTypes.string,
    children: PropTypes.node
};

export { PreviewCard, PreviewAltCard, PreviewTable, CodeBlock };
