import React from 'react';
import PropTypes from 'prop-types';
import { TooltipComponent } from '../Component';
import { Label } from 'reactstrap';

function Tel ({ fieldLabel, fieldName, fieldId, fieldRequired, fieldValue, fieldReadOnly, fieldDisabled, fieldPlaceHolder, fieldToolTip, fieldToolTipContent, ...props }) {
    return (
        <div className="form-group">
            <Label
                className="form-label"
                htmlFor={fieldId}
            >
                {fieldLabel}
                {' '}
                {
                    fieldRequired && (
                        <sup className="text-danger">*</sup>
                    )
                }
            </Label>

            {
                fieldToolTip && (
                    <>
                        <TooltipComponent
                            icon="help-fill"
                            iconClass="card-hint text-secondary"
                            direction="top"
                            id="tooltip-1"
                            text={fieldToolTipContent}
                        />
                    </>
                )
            }
            <div className="form-control-wrap">
                <input
                    ref={props.register({
                        required: !!fieldRequired,
                        pattern: {
                            value: /^\d{10}$/,
                            message: 'Invalid phone number'
                        }
                    })}
                    type="tel"
                    id={fieldId}
                    name={fieldName}
                    className={`form-control date-picker ${props.errors[fieldName] ? 'is-invalid' : ''}`}
                    autoComplete="new-password"
                    defaultValue={fieldValue || ''}
                    placeholder={fieldPlaceHolder}
                    readOnly={fieldReadOnly}
                    disabled={fieldDisabled}
                />
                {props.errors[fieldName] && props.errors[fieldName].type === 'required' && (
                    <span className="invalid">
                        {fieldLabel} is required
                    </span>
                )}

                {props.errors[fieldName] && props.errors[fieldName].type === 'pattern' && (
                    <span className="invalid">
                        {fieldLabel} is invalid
                    </span>
                )}
            </div>
        </div>
    );
}

Tel.propTypes = {
    fieldLabel: PropTypes.string,
    fieldName: PropTypes.string,
    fieldId: PropTypes.string,
    fieldRequired: PropTypes.bool,
    fieldValue: PropTypes.string,
    fieldReadOnly: PropTypes.bool,
    fieldDisabled: PropTypes.bool,
    fieldToolTip: PropTypes.bool,
    fieldToolTipContent: PropTypes.string,
    register: PropTypes.func,
    errors: PropTypes.object,
    fieldPlaceHolder: PropTypes.string
};

export default Tel;
