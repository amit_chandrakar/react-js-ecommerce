import React from 'react';
import PropTypes from 'prop-types';
import { TooltipComponent } from '../Component';
import { Label } from 'reactstrap';

function Email ({ fieldLabel, fieldName, fieldId, fieldRequired, fieldValue, fieldReadOnly, fieldDisabled, fieldPlaceHolder, fieldToolTip, fieldToolTipContent, ...props }) {
    return (
        <div className="form-group">
            <Label
                className="form-label"
                htmlFor={fieldId}
            >
                {fieldLabel}
            </Label>
            {' '}
            {
                fieldRequired && (
                    <sup className="text-danger">*</sup>
                )
            }

            {
                fieldToolTip && (
                    <>
                        <TooltipComponent
                            icon="help-fill"
                            iconClass="card-hint text-secondary"
                            direction="top"
                            id="tooltip-1"
                            text={fieldToolTipContent}
                        />
                    </>
                )
            }

            <div className="form-control-wrap">
                <input
                    ref={
                        props.register &&
                        props.register({
                            required: !!fieldRequired,
                            pattern: {
                                value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                                message: 'Invalid email address'
                            }
                        })
                    }
                    type="text"
                    id={fieldId}
                    name={fieldName}
                    className={`form-control date-picker ${props.errors[fieldName] ? 'is-invalid' : ''}`}
                    autoComplete="new-password"
                    defaultValue={fieldValue}
                    placeholder={fieldPlaceHolder}
                    readOnly={fieldReadOnly}
                    disabled={fieldDisabled}
                />
                {props.errors[fieldName] && props.errors[fieldName].type === 'required' && (
                    <span className="invalid">
                        {fieldLabel} is required
                    </span>
                )}

                {props.errors[fieldName] && props.errors[fieldName].type === 'pattern' && (
                    <span className="invalid">
                        {props.errors[fieldName].message}
                    </span>
                )}
            </div>
        </div>
    );
}

Email.propTypes = {
    fieldLabel: PropTypes.string,
    fieldName: PropTypes.string,
    fieldId: PropTypes.string,
    fieldRequired: PropTypes.bool,
    fieldValue: PropTypes.string,
    fieldReadOnly: PropTypes.bool,
    fieldDisabled: PropTypes.bool,
    fieldPlaceHolder: PropTypes.string,
    fieldToolTip: PropTypes.bool,
    fieldToolTipContent: PropTypes.string,
    register: PropTypes.func,
    errors: PropTypes.object
};

export default Email;
