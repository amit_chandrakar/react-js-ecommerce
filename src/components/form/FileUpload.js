import React from 'react';
import PropTypes from 'prop-types';
import { TooltipComponent } from '../Component';
import { Button, Label } from 'reactstrap';
import Dropzone from 'react-dropzone';

function FileUpload ({
    fieldLabel,
    fieldName,
    fieldId,
    fieldRequired,
    fieldDefaultFile,
    fieldToolTip,
    fieldToolTipContent,
    fieldFiles,
    handleDropChange,
    ...props
}) {
    return (
        <div className="form-group">
            <Label className="form-label" htmlFor="image-dropzone">
                {fieldLabel}{' '}
                {fieldRequired && <sup className="text-danger">*</sup>}
                {fieldToolTip && (
                    <>
                        <TooltipComponent
                            icon="help-fill"
                            iconClass="card-hint text-secondary"
                            direction="top"
                            id="tooltip-1"
                            text={fieldToolTipContent}
                        />
                    </>
                )}
            </Label>
            <Dropzone
                onDrop={(acceptedFiles) => handleDropChange(acceptedFiles)}
                accept={['.jpg', '.png', '.jpeg']}
                maxFiles={1}
                id="image-dropzone"
            >
                {({ getRootProps, getInputProps }) => (
                    <section>
                        <div
                            {...getRootProps()}
                            className="dropzone upload-zone dz-clickable"
                        >
                            <input {...getInputProps()} />
                            {(fieldFiles.length === 0 && !fieldDefaultFile) && (
                                <div className="dz-message">
                                    <span className="dz-message-text">
                                        Drag and drop file
                                    </span>
                                    <span className="dz-message-or">or</span>
                                    <Button color="primary" type="button">
                                        SELECT
                                    </Button>
                                </div>
                            )}
                            {fieldFiles.length === 0 && fieldDefaultFile && (
                                <div className="dz-preview dz-processing dz-image-preview dz-error dz-complete">
                                    <div className="dz-image">
                                        <img
                                            src={fieldDefaultFile}
                                            alt="default-preview"
                                        />
                                    </div>
                                </div>
                            )}
                            {fieldFiles.map((file) => (
                                <div
                                    key={file.name}
                                    className="dz-preview dz-processing dz-image-preview dz-error dz-complete"
                                >
                                    <div className="dz-image">
                                        <img src={file.preview} alt="preview" />
                                    </div>
                                </div>
                            ))}
                        </div>
                    </section>
                )}
            </Dropzone>
        </div>
    );
}

FileUpload.propTypes = {
    fieldLabel: PropTypes.string,
    fieldName: PropTypes.string,
    fieldId: PropTypes.string,
    fieldRequired: PropTypes.bool,
    fieldValue: PropTypes.string,
    fieldReadOnly: PropTypes.bool,
    fieldDisabled: PropTypes.bool,
    fieldPlaceHolder: PropTypes.string,
    fieldToolTip: PropTypes.bool,
    fieldToolTipContent: PropTypes.string,
    register: PropTypes.func,
    errors: PropTypes.object,
    fieldDefaultFile: PropTypes.string,
    fieldFiles: PropTypes.array,
    handleDropChange: PropTypes.func
};

export default FileUpload;
