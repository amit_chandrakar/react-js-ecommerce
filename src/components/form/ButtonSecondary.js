import React from 'react';
import { Button } from 'reactstrap';
import { Icon } from '../Component';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';

function ButtonSecondary ({ icon, fieldLabel, redirectUrl }) {
    const history = useHistory();
    const defaultIcon = icon || 'arrow-left';
    const defaultLabel = fieldLabel || 'Save';

    const handleButtonClick = () => {
        if (redirectUrl) {
            redirectUrl === 'go-back' ? history.goBack() : history.push(redirectUrl);
        }
    };

    return (
        <div className="form-group">
            <Button
                color="light"
                outline
                className="bg-white d-none d-sm-inline-flex"
                onClick={handleButtonClick}
            >
                <Icon name={defaultIcon}></Icon>
                <span>{defaultLabel}</span>
            </Button>
            <a
                href="#"
                onClick={handleButtonClick}
                className="btn btn-icon btn-outline-light bg-white d-inline-flex d-sm-none"
            >
                <Icon name={defaultIcon}></Icon>
            </a>
        </div>
    );
}

ButtonSecondary.propTypes = {
    icon: PropTypes.string,
    fieldLabel: PropTypes.string,
    redirectUrl: PropTypes.string
};

export default ButtonSecondary;
