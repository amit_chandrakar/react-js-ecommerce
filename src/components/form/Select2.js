import React from 'react';
import Select from 'react-select';
import PropTypes from 'prop-types';
import { Label } from 'reactstrap';
import { TooltipComponent } from '../Component';

function Select2 ({ fieldLabel, fieldName, fieldRequired, fieldValue, fieldToolTip, fieldToolTipContent, fieldOptions, isMulti, ...props }) {
    return (
        <div className="form-group">
            <Label className="form-label" htmlFor="fv-status">
                {fieldLabel}
                {' '}
                {
                    fieldRequired && (
                        <sup className="text-danger">*</sup>
                    )
                }
            </Label>

            {
                fieldToolTip && (
                    <>
                        {' '}
                        <TooltipComponent
                            icon="help-fill"
                            iconClass="card-hint text-secondary"
                            direction="top"
                            id="tooltip-1"
                            text={fieldToolTipContent}
                        />
                    </>
                )
            }

            <div className="form-control-select">
                <Select
                    {...props.register(fieldName, { required: fieldRequired })}
                    className={`react-select-container ${props.className ? props.className : ''}`}
                    classNamePrefix="react-select"
                    options={fieldOptions}
                    isMulti={isMulti || false}
                    {...props}
                />
                {props.errors[fieldName] && props.errors[fieldName].type === 'required' && (
                    <span className="invalid">
                        {fieldLabel} is required
                    </span>
                )}
            </div>
        </div>
    );
}

Select2.propTypes = {
    fieldLabel: PropTypes.string,
    fieldName: PropTypes.string,
    fieldRequired: PropTypes.bool,
    fieldValue: PropTypes.string,
    fieldToolTip: PropTypes.bool,
    fieldToolTipContent: PropTypes.string,
    fieldOptions: PropTypes.array,
    register: PropTypes.func,
    errors: PropTypes.object,
    setValue: PropTypes.func,
    className: PropTypes.string,
    isMulti: PropTypes.bool
};

export default Select2;
