import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Icon, TooltipComponent } from '../Component';
import { Label } from 'reactstrap';

function Password ({ fieldLabel, fieldName, fieldId, fieldRequired, fieldValue, fieldReadOnly, fieldDisabled, fieldToolTip, fieldToolTipContent, ...props }) {
    const [passState, setPassState] = useState(false);

    const validatePassword = (value) => {
        const hasUpperCase = /[A-Z]/.test(value);
        const hasLowerCase = /[a-z]/.test(value);
        const hasNumber = /\d/.test(value);

        if (value.length < 6) {
            return 'Password must be at least 6 characters long';
        }

        if (!(hasUpperCase && hasLowerCase && hasNumber)) {
            return 'Password must contain 1 uppercase & 1 lowercase letter, and 1 number';
        }

        return true;
    };

    return (
        <div className="form-group">
            <div className="form-label-group">
                <Label
                    className="form-label"
                    htmlFor={fieldId}
                >
                    {fieldLabel}
                    {' '}
                    {
                        fieldRequired && (
                            <sup className="text-danger">*</sup>
                        )
                    }
                </Label>

                {
                    fieldToolTip && (
                        <>
                            <TooltipComponent
                                icon="help-fill"
                                iconClass="card-hint text-secondary"
                                direction="top"
                                id="tooltip-1"
                                text={fieldToolTipContent}
                            />
                        </>
                    )
                }
            </div>
            <div className="form-control-wrap">
                <a
                    href={`#${fieldId}`}
                    onClick={(ev) => {
                        ev.preventDefault();
                        setPassState(!passState);
                    }}
                    className={`form-icon form-icon-right passcode-switch ${passState ? 'is-hidden' : 'is-shown'
                    }`}
                >
                    <Icon
                        name="eye"
                        className="passcode-icon icon-show"
                    ></Icon>

                    <Icon
                        name="eye-off"
                        className="passcode-icon icon-hide"
                    ></Icon>
                </a>
                <input
                    type={passState ? 'text' : 'password'}
                    id={fieldId}
                    name={fieldName}
                    defaultValue={fieldValue || ''}
                    ref={props.register({
                        required: !!fieldRequired,
                        validate: validatePassword
                    })}
                    autoComplete="new-password"
                    className={`form-control form-control ${props.errors[fieldName] ? 'is-invalid' : ''} ${passState ? 'is-hidden' : 'is-shown'
                    }`}
                />
                {props.errors[fieldName] && props.errors[fieldName].type === 'required' && (
                    <span className="invalid">
                        {fieldLabel} is required
                    </span>
                )}

                {props.errors[fieldName] && props.errors[fieldName].message && (
                    <span className="invalid">
                        {props.errors[fieldName].message}
                    </span>
                )}
            </div>
        </div>
    );
}

Password.propTypes = {
    fieldLabel: PropTypes.string,
    fieldName: PropTypes.string,
    fieldId: PropTypes.string,
    fieldRequired: PropTypes.bool,
    fieldValue: PropTypes.string,
    fieldReadOnly: PropTypes.bool,
    fieldDisabled: PropTypes.bool,
    fieldToolTip: PropTypes.bool,
    fieldToolTipContent: PropTypes.string,
    register: PropTypes.func,
    errors: PropTypes.object
};

export default Password;
