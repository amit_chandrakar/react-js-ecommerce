import React from 'react';
import { Button, Spinner } from 'reactstrap';
import { Icon } from '../Component';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';

function ButtonPrimary ({ icon, fieldLabel, formLoading, formLoadingLabel, redirectUrl }) {
    const history = useHistory();
    const defaultIcon = icon || 'check';
    const defaultLabel = fieldLabel || 'Save';

    return (
        <div className="form-group">
            <Button
                color="primary"
                onClick={redirectUrl ? () => history.push(redirectUrl) : undefined}
            >
                {formLoading
                    ? (
                        <>
                            <Spinner size="sm" color="light" />
                            <span>{formLoadingLabel}</span>
                        </>
                    )
                    : (
                        <>
                            <Icon name={defaultIcon}></Icon>
                            <span>{defaultLabel}</span>
                        </>
                    )
                }
            </Button>
        </div>
    );
}

ButtonPrimary.propTypes = {
    fieldLabel: PropTypes.string,
    formLoading: PropTypes.bool,
    icon: PropTypes.string,
    formLoadingLabel: PropTypes.string,
    redirectUrl: PropTypes.string
};

export default ButtonPrimary;
