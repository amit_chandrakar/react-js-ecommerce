import React from 'react';
import Icon from '../icon/Icon';
import classNames from 'classnames';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

const LinkItem = ({ ...props }) => {
    return (
        <li>
            {props.tag !== 'a'
                ? (
                    <Link to={process.env.PUBLIC_URL + props.link} {...props}>
                        {props.icon ? <Icon name={props.icon} /> : null} <span>{props.text || props.children}</span>
                    </Link>
                )
                : (
                    <a href={process.env.PUBLIC_URL + props.link} onClick={(ev) => ev.preventDefault()}>
                        {props.icon ? <Icon name={props.icon} /> : null} <span>{props.text || props.children}</span>
                    </a>
                )}
        </li>
    );
};

LinkItem.propTypes = {
    link: PropTypes.string,
    icon: PropTypes.string,
    text: PropTypes.string,
    tag: PropTypes.string,
    children: PropTypes.node
};

const LinkList = ({ ...props }) => {
    const listClasses = classNames({
        'link-list': !props.opt,
        'link-list-opt': props.opt,
        [`${props.className}`]: props.className
    });
    return <ul className={listClasses}>{props.children}</ul>;
};

LinkList.propTypes = {
    opt: PropTypes.bool,
    className: PropTypes.string,
    children: PropTypes.node
};

export { LinkItem, LinkList };
