import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

const PageContainer = ({ ...props }) => {
    const [themeState] = useState({
        main: 'default',
        sidebar: 'dark',
        header: 'white',
        skin: 'light'
    });
    // const [themeState] = useState({
    //     main: 'dark',
    //     sidebar: 'dark',
    //     header: 'dark',
    //     skin: 'dark'
    // });

    useEffect(() => {
        document.body.className = `nk-body bg-lighter npc-default has-sidebar no-touch nk-nio-theme ${
            themeState.skin === 'dark' ? 'dark-mode' : ''
        }`;
    }, [themeState.skin]);

    return (
        <React.Fragment>
            <div className="nk-app-root">
                <div className="nk-wrap nk-wrap-nosidebar">
                    <div className="nk-content">{props.children}</div>
                </div>
            </div>
        </React.Fragment>
    );
};

PageContainer.propTypes = {
    children: PropTypes.node
};

export default PageContainer;
