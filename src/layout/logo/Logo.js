import React from 'react';
import LogoDefault from '../../images/logo.png';
import { Link } from 'react-router-dom';

const Logo = () => {
    return (
        <Link to={`${process.env.PUBLIC_URL}/account/`} className="logo-link">
            <img className="logo-light logo-img" src={LogoDefault} alt="logo" />
            <img className="logo-dark logo-img" src={LogoDefault} alt="logo" />
        </Link>
    );
};

export default Logo;
