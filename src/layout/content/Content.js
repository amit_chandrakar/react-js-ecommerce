import React from 'react';
import PropTypes from 'prop-types';

const Content = ({ ...props }) => {
    const CardSize = (props.CardSize && props.CardSize === 'xl') ? '' : 'wide-md mx-auto';

    return (
        <div className="nk-content">
            <div className="container-fluid">
                <div className="nk-content-inner">
                    <div className="nk-content-body">
                        {!props.page ? props.children : null}
                        {props.page === 'component'
                            ? (
                                <div className={`components-preview ${CardSize}`}>{props.children}</div>
                            )
                            : null}
                    </div>
                </div>
            </div>
        </div>
    );
};

Content.propTypes = {
    children: PropTypes.node,
    page: PropTypes.string,
    CardSize: PropTypes.string
};

export default Content;
