import React from 'react';
import classNames from 'classnames';
import Toggle from '../sidebar/Toggle';
import Logo from '../logo/Logo';
import PageTitle from '../page-title/PageTitle';
import User from './dropdown/user/User';
import PropTypes from 'prop-types';

const Header = ({ fixed, theme, className, setVisibility, ...props }) => {
    const headerClass = classNames({
        'nk-header': true,
        'nk-header-fixed': fixed,
        'is-light': theme === 'white',
        [`is-${theme}`]: theme !== 'white' && theme !== 'light',
        [`${className}`]: className
    });
    return (
        <div className={headerClass}>
            <div className="container-fluid">
                <div className="nk-header-wrap">
                    <div className="nk-menu-trigger d-xl-none ms-n1">
                        <Toggle
                            className="nk-nav-toggle nk-quick-nav-icon d-xl-none ms-n1"
                            icon="menu"
                            click={props.sidebarToggle}
                        />
                    </div>
                    <div className="nk-header-brand d-xl-none">
                        <Logo />
                    </div>
                    <div className="nk-header-news d-none d-xl-block">
                        <PageTitle />
                    </div>
                    <div className="nk-header-tools">
                        <ul className="nk-quick-nav">
                            <li
                                className="user-dropdown"
                                onClick={() => setVisibility(false)}
                            >
                                <User />
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    );
};

Header.propTypes = {
    fixed: PropTypes.bool,
    theme: PropTypes.string,
    className: PropTypes.string,
    setVisibility: PropTypes.func,
    title: PropTypes.string,
    changeTheme: PropTypes.func,
    sidebarToggle: PropTypes.func
};

export default Header;
