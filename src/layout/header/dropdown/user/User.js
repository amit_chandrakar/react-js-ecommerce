import React, { useState } from 'react';
import UserAvatar from '../../../../components/user/UserAvatar';
import { DropdownToggle, DropdownMenu, Dropdown } from 'reactstrap';
import { Icon } from '../../../../components/Component';
import { LinkList, LinkItem } from '../../../../components/links/Links';
import { findUpper } from '../../../../utils/Constants';

const User = () => {
    const [open, setOpen] = useState(false);
    const toggle = () => setOpen((prevState) => !prevState);
    let user = localStorage.getItem('loggedInUser');
    user = JSON.parse(user);

    const handleSignout = () => {
        localStorage.removeItem('accessToken');
    };

    return (
        <Dropdown isOpen={open} className="user-dropdown" toggle={toggle}>
            <DropdownToggle
                tag="a"
                href="#toggle"
                className="dropdown-toggle"
                onClick={(ev) => {
                    ev.preventDefault();
                }}
            >
                <div className="user-toggle">
                    <UserAvatar icon="user-alt" className="sm" />
                    <div className="user-info d-none d-md-block">
                        <div className="user-status">Administrator</div>
                        <div className="user-name dropdown-indicator">{user.name}</div>
                    </div>
                </div>
            </DropdownToggle>
            <DropdownMenu end className="dropdown-menu-md dropdown-menu-s1">
                <div className="dropdown-inner user-card-wrap bg-lighter d-none d-md-block">
                    <div className="user-card sm">
                        <div className="user-avatar">
                            <span>{findUpper(user.name)}</span>
                        </div>
                        <div className="user-info">
                            <span className="lead-text">{user.name}</span>
                            <span className="sub-text">{user.email}</span>
                        </div>
                    </div>
                </div>
                <div className="dropdown-inner">
                    <LinkList>
                        <LinkItem link="/account/user-profile-regular" icon="user-alt" onClick={toggle}>
              View Profile
                        </LinkItem>
                        <LinkItem link="/account/user-profile-setting" icon="setting-alt" onClick={toggle}>
              Account Setting
                        </LinkItem>
                        <LinkItem link="/account/user-profile-activity" icon="activity-alt" onClick={toggle}>
              Login Activity
                        </LinkItem>
                        <LinkItem link="#" icon="icon ni ni-moon">
                Dark Mode
                        </LinkItem>
                    </LinkList>
                </div>
                <div className="dropdown-inner">
                    <LinkList>
                        <a href={`${process.env.PUBLIC_URL}/login`} onClick={handleSignout}>
                            <Icon name="signout"></Icon>
                            <span>Sign Out</span>
                        </a>
                    </LinkList>
                </div>
            </DropdownMenu>
        </Dropdown>
    );
};

export default User;
