import React from 'react';
import { Switch, Route, withRouter } from 'react-router-dom';
import { RedirectAs404 } from './utils/Utils';
import PrivateRoute from './route/PrivateRoute';
import Layout from './layout/Index';
import Login from './pages/auth/Login';
import Register from './pages/auth/Register';
import ForgotPassword from './pages/auth/ForgotPassword';
import Success from './pages/auth/Success';
import { TitleProvider } from './contexts/TitleContext';

const App = () => {
    return (
        <TitleProvider>
            <Switch>
                {/* Auth Pages */}
                <Route exact path={`${process.env.PUBLIC_URL}/account/auth-success`} component={Success}></Route>
                <Route exact path={`${process.env.PUBLIC_URL}/account/auth-reset`} component={ForgotPassword}></Route>
                <Route exact path={`${process.env.PUBLIC_URL}/account/auth-register`} component={Register}></Route>
                <Route exact path={`${process.env.PUBLIC_URL}/login`} component={Login}></Route>

                {/* Main Routes */}
                <PrivateRoute exact path="" component={Layout}></PrivateRoute>
                <Route component={RedirectAs404}></Route>
            </Switch>
        </TitleProvider>
    );
};
export default withRouter(App);
